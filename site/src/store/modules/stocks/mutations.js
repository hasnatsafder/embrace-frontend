export default {
  setStocks(state, response) {
    state.stocks = response.data
    state.stocks.forEach((stock) => {
      stock.rvolume = parseInt(stock.volume * stock.price)
    })
  },

  setHistoryData(state, response) {
    state.historyData = response.data
    // state.historyData.stock = state.historyData.stock.reverse()
    // console.log(state.historyData.stock.map(a => a.price))
    // console.log(['sep', 'oct', 'nov', 'dec', 'jan'])
    // // state.historyData.stock.each
    // state.historyData.indexes = state.historyData.indexes.reverse()
  },

  setAveragesData(state, response) {
    state.stockAverages = response.data
  },

  setStockDetail(state, response) {
    state.stockDetail = response.data
  }
}