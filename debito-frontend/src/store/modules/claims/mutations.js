export default {
  addToFilesBuffer(state, filesInfo) {
    var files = filesInfo.files
    var props = Object.keys(files)
    for (var i = 0; i < props.length; i++) {
      let file = files[i]
      state.acceptedTypesError = false
      if (file === undefined) {
        return
      }

      const fileType = "." + file.name.split('.').pop()
      if (state.acceptedTypes.includes(fileType.toLowerCase())) {
        
        let invoiceDate = ""
        let maturityDate = ""
        let invoiceReference = ""
        let amount = ""
        console.log(filesInfo)
        // if source is payment we need to set maturity date current date
        if (filesInfo.bufferType == "payment") {
          maturityDate = new Date()
        }
        if (filesInfo.bufferType == "document") {
          invoiceDate = new Date()
          maturityDate = new Date()
          amount = "0"
        }

        let newQueue = {
          invoice_date: invoiceDate,
          maturity_date: maturityDate,
          invoice_reference: invoiceReference,
          amount: amount,
          claim_line_type_id: filesInfo.claim_line_type_id,
          file: file
        }
        state.fileBuffer.push(newQueue)
      } else {
        state.acceptedTypesError = true
      }
    }
  },
  removeFile(state, fileInfo) {
    fileInfo.file.splice(fileInfo.file.indexOf(fileInfo.index), 1)
  },
  setClaimSuccessPage(state) {
    state.claimSuccessPage = true
  },
  resetClaimSuccessPage(state) {
    state.claimSuccessPage = false
  },
  setClaimErrors(state) {
    state.claimError = true
    state.reprecenter = null
    state.disputes = false
    state.dispute = ''
  },
  resetClaimsState(state) {
    state.fileQueue = []
    state.reprecenter = null
    state.disputes = false
    state.dispute = ''
    state.fileBuffer = []
    state.fileQueueInvoice = []
    state.fileQueuePayemnt = []
    state.fileQueueReminder = []
    state.fileQueueOther = []
    state.allFiles = []
    state.selectedDebtor = []
    state.claim_id = ""
    state.accept_charge = null
    state.accept_payment = null
    state.accept_reminder = null
    state.accept_other = null
    state.sendingData = false
  },
  setClaimError(state, val) {
    state.claimError = val
  },
  setRepresenter(state, val) {
    state.reprecenter = val
  },
  setDisputes(state, val) {
    state.disputes = val
  },
  setDispute(state, val) {
    state.dispute = val
  },
  resetClaims(state) {
    state.claims = []
  },
  setClaims(state, data) {
    let claimsData = Object.values(data.data)
    claimsData.forEach(claim => {
      if (claim.Debtors.company_name) {
        claim.full_name = claim.Debtors.company_name
      } else if (claim.Debtors.first_name && claim.Debtors.last_name) {
        claim.full_name = claim.Debtors.first_name + " " + claim.Debtors.last_name
      } else {
        claim.full_name = claim.Debtors.first_name
      }
    })
    state.claims = data
    state.claims.data = claimsData
  },
  resetClaimPhases(state) {
    state.claimPhases = []
  },
  setClaimPhases(state, data) {
    state.claimPhases = data
  },
  setTableLoading(state, val) {
    state.tableLoading = val
  },
  setSingleClaim(state, val) {
    if (val.data.debtor.company_name) {
      val.data.debtor.full_name = val.data.debtor.company_name
    } else if (val.data.debtor.first_name && val.data.debtor.last_name) {
      val.data.debtor.full_name = val.data.debtor.first_name + " " + val.data.debtor.last_name
    } else {
      val.data.debtor.full_name = val.data.debtor.first_name
    }
    state.singleClaim = val
  },
  setSingleClaimLines(state, val) {
    state.claimLines = val
  },
  resetSingleClaimLines(state) {
    state.claimLines = []
  },
  setClaimActions(state, val) {
    state.claimActions = val
  },
  resetClaimActions(state) {
    state.claimActions = []
  },
  setClaimActionTypes(state, val) {
    state.claimActionTypes = val
  },
  resetClaimActionTypes(state) {
    state.claimActionTypes = []
  },
  resetSingleClaim(state) {
    state.singleClaim = []
  },
  setclaimLineTypes(state, val) {
    state.claimLineTypes = val
  },
  setClaimsCount(state, val) {
    state.count = val.data[0].count
  },
  resetClaimsCount(state) {
    state.count = "-"
  },
  setClaimFinancials(state, val) {
    state.claimFinancials = val
  },
  resetClaimFinancials(state) {
    state.claimFinancials = []
  },
  setAcceptCharge(state, val) {
    state.accept_charge = val
  },
  setPaymentCharge(state, val) {
    state.accept_payment = val
  },
  setReminderCharge(state, val) {
    state.accept_reminder = val
  },
  setOtherCharge(state, val) {
    state.accept_other = val
  },
  resetBuffer(state) {
    state.fileBuffer = []
  },
  setCurrencies(state, val) {
    state.currencies = val
  },
  setCurrentCurrency(state, val) {
    state.currentCurrency = val
  },
  submitBufferFiles(state, file) {
    for (let obj in state.fileBuffer) {
      file.push(state.fileBuffer[obj])
    }
    state.fileBuffer = []
  },
  setSelectedDebtor(state, obj) {
    state.selectedDebtor = obj
  },
  pustToAllFiles(state, obj) {
    state.allFiles.push(obj)
  },
  resetAllFiles(state) {
    state.allFiles = []
  },
  setClaimId(state, val) {
    state.claim_id = val
  },
  setDebtorLawyer(state, val) {
    state.debtorLawyer = val
  },
  resetLawyerInput(state) {
    state.lawyerInput.name = null
    state.lawyerInput.address = null
    state.lawyerInput.zip_code = null
    state.lawyerInput.city = null
    state.lawyerInput.phone = null
    state.lawyerInput.vat_number = null
    state.lawyerInput.email = null
  },
  setSendingData(state, val) {
    state.sendingData = val
  },
  resetAllClaimActions(state) {
    state.claimActionsAll = {}
  },
  setAllClaimActions(state, data) {
    state.claimActionsAll = data.data.actions
    state.claimActionsAll = state.claimActionsAll.sort((a, b) => b.created.localeCompare(a.created))
  },
}
