
export default {
  clearNameSearchResult(state) {
    state.searchInput.nameSearchingStatus = false
    state.searchInput.searchingName = false
  },
  clearVatSearchResult(state) {
    state.searchInput.vatSearchingStatus = false
    state.searchInput.searchingVat = false
  }
}
