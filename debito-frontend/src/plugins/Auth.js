const AuthPlugin = {
  setIntercomInfo (formInput) {
    // As of now formInput is just the email being used on login (15/10-18)
    sessionStorage.setItem('intercom', JSON.stringify({
      name: formInput,
      email: formInput
    }))
  },
  getIntercomInfo () {
    if (sessionStorage.getItem('intercom')) {
      return JSON.parse(sessionStorage.getItem('intercom'))
    }
  },
  setToken (token, expiration) {
    localStorage.setItem('authToken', token)
    localStorage.setItem('authTokenExpiration', expiration)
  },
  destroyToken () {
    localStorage.removeItem('authToken')
    localStorage.removeItem('authTokenExpiration')
    sessionStorage.removeItem('intercom')
  },
  getToken () {
    const token = localStorage.getItem('authToken')
    const expiration = localStorage.getItem('authTokenExpiration')

    if (!token || !expiration) {
      return null
    }

    if (Date.now() > parseInt(expiration)) {
      this.destroyToken()
      return null
    } else {
      return token
    }
  },
  loggedIn () {
    return this.getToken()
  }
}

export default function(Vue) {
  Vue.auth = AuthPlugin

  Object.defineProperties(Vue.prototype, {
    $auth: {
      get () {
        return Vue.auth
      }
    }
  })
}
