import actions from './actions.js'
import mutations from './mutations.js'

export default {
  namespaced: true,
  state: {
    searchInput: {
      searchingVat: false,
      vatSearchingStatus: false,
      searchingName: false,
      nameSearchingStatus: false
    }
  },
  actions,
  mutations,
}
