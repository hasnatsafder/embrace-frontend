# Public Debito API

Base URL: /api/v1/


## Authentication

- /auth/register
- /auth/token

## Authenticated

- /accounts - GET, POST
- /accounts/:account_id - GET, PUT, DELETE

- /accounts/:account_id/users - GET, POST
- /accounts/:account_id/users/:user_id - PUT, DELETE

- /claims - GET, POST
- /claims/:claim_id - GET, PUT, DELETE

- /claims/:claim_id/claim-lines - GET, POST
- /claims/:claim_id/claim-lines/:claim_line_id - GET, PUT, DELETE

- /claims/:claim_id/claim-actions - GET
- /claims/:claim_id/claim-actions/:claim_action_id - GET

- /claims/:claim_id/claim-financials - GET
- /claims/:claim_id/claim-financials/:claim_financial_id - GET

- /claims/:claim_id/claim-payments - GET
- /claims/:claim_id/claim-payments/:claim_payment_id - GET

- /claim-line-types/ - GET
- /claim-line-types/:claim_line_type_id - GET

- /claim-phases/ - GET
- /claim-phases/:claim_phase_id - GET

- /claim-types/ - GET
- /claim-types/:claim_type_id - GET

- /claim-action-types/ - GET
- /claim-action-types/:claim_action_type_id - GET

- /company-types - GET
- /company-types/:company_type_id - GET

- /currencies - GET
- /currencies/:currency_id - GET

- /countries - GETc
- /countries/:country_id - GET

- /debtors - GET, POST
- /debtors/:debtor_id - GET, PUT, DELETE

- /roles - GET
- /roles/:roles_id - GET

- /users/ - GET
- /users/:user_id - GET, PUT




# Non-authenticated (TODO!)

- /claims/create-from-website - POST w/ form data
	- email



# ROOT INTERFACE (TODO!)

Base url: /admin/

- /users
	- base details
	- change email/password
	- accounts

- /accounts
	- base details
	- collectors details
	- users

- /claims/not-synced - GET

- /claims/:claim_id - GET/PUT
	- Select or add debtor (in this account!)
	- Set customer reference
	- Add claim action
		- Missing debtor details
		- Missing invoice detals 
		- Misisng ....
	- Add/edit claim lines
		- Enter base details (claim_line_type, currency, invoice_date, maturity_date, amount, invoice_reference)

## 100% CRUD things
- /company-types
- /roles
- /collectors
- /currencies 
- /countries

- /claim-types
- /claim-action-types
- /claim-phases

- /claim-line-types

# The TODO list

- Make the API docs
- Upload files to claims
- Finish the admin/root interface
- "Activate" flow when registering users
- "Forgotten password" flow in /auth/forgot-password
- `is_active` flag on Users
- OAuth2 and control who can use the API from /root
- Validation rule to set the amount to negative or positive for specific claim_line_type_id
- /claims/create-from-website

## Common actions
- Set active account 
  - HTTP PUT to /users/:user_id 
  - Header key value `active_account_id` 

- Create claims
  - HTTP POST to /claims/

- Create debtors
  - HTTP POST to /debtors/

/claims?include=Accounts
/claims/:claim_id:?include=ClaimLines`

