import actions from './actions'
import mutations from './mutations'

export default {
  namespaced: true,
  state: {
    stocks: [],
    stockDetail : [],
    historyData: {
      stock: [],
      kse100: []
    },
    stockAverages: []
  },
  actions,
  mutations
}