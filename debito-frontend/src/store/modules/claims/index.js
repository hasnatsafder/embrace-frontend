import actions from "./actions.js"
import mutations from "./mutations.js"

export default {
  namespaced: true,
  state: {
    claims: [],
    fileBuffer: [],
    claimActionsAll: {},
    fileQueueInvoice: [],
    fileQueuePayemnt: [],
    fileQueueReminder: [],
    fileQueueOther: [],
    allFiles: [],
    acceptedTypes: [
      ".pdf",
      ".xls",
      ".xlsx",
      ".doc",
      ".docx",
      ".png",
      ".jpg",
      ".jpeg",
      ".tiff",
      ".csv"
    ],
    acceptedTypesError: false,
    disputes: false,
    dispute: "",
    claimSuccessPage: false,
    claimError: false,
    reprecenter: false,
    tableLoading: false,
    claimPhases: [],
    singleClaim: [],
    claimLines: [],
    claimLineTypes: [],
    claimActions: [],
    claimActionTypes: [],
    claimFinancials: [],
    count: "-",
    accept_charge: null,
    accept_payment: null,
    accept_reminder: null,
    accept_other: null,
    currency: "DKK",
    currencies: [],
    currentCurrency: "",
    claim_line_types: [],
    selectedDebtor: [],
    claim_id: "",
    lawyerInput: {
      name: null,
      address: null,
      zip_code: null,
      city: null,
      phone: null,
      vat_number: null,
      email: null,
    },
    debtorLawyer: false,
    sendingData: false
  },
  getters: {
    // return priorty currencies to be displayed on top in select box (such as danish etc)
    priorityCurrenciesArray: function(state) {
      let priortyArr = state.currencies.data

      if (!priortyArr) {
        return []
      }

      // selecting all currencies to be shown on top
      let danishKrone = priortyArr.filter(d => d.iso_code == "DKK")
      let euro = priortyArr.filter(d => d.iso_code == "EUR")
      let usDollor = priortyArr.filter(d => d.iso_code == "USD")
      let pound = priortyArr.filter(d => d.iso_code == "GBP")
      let norwayKrone = priortyArr.filter(d => d.iso_code == "NOK")
      let swedishKrone = priortyArr.filter(d => d.iso_code == "SEK")

      // appending preferred currencies to top of array
      priortyArr.unshift(swedishKrone[0])
      priortyArr.unshift(norwayKrone[0])
      priortyArr.unshift(pound[0])
      priortyArr.unshift(usDollor[0])
      priortyArr.unshift(euro[0])
      priortyArr.unshift(danishKrone[0])

      // remove Duplicates
      priortyArr = [...new Set(priortyArr)]
      let priortyArrFull = []
      priortyArr.forEach(function(element, index) {
        priortyArrFull.push({ value: element.id, text: (element.name + " - " + element.iso_code) })
      })
      return priortyArrFull
    },
    // return total claims
    totalClaims: function(state) {
      if (state.claims.pagination) {
        return state.claims.pagination.count
      }
      return 0
    },
    // return total amount
    totalAmount: function(state) {
      var total = 0
      if (state.claims.data) {
        state.claims.data.forEach(element => {
          total = total + parseFloat(element.claimLinesSum) - parseFloat(element.claimLinesDeduct)
        })
        return total
      } else {
        return 0
      }
    }
  },
  actions,
  mutations
}
