import moment from 'moment'
import { bus } from '../main.js'
import { ToastProgrammatic as Toast } from 'buefy'
var toast = false

// Count the length of an object
const countObjectProperties = obj => {
  if (typeof obj === 'object') {
    return Object.keys(obj).length
  }
  return 0
}

// Front end Valid Email syntax checker
const validEmail = email => {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(email)
}

// Used in inputs, to resrtict numbers only
const allowNumbers = e => {
  var key = e.which || e.keyCode

  if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
  // numbers
      key >= 48 && key <= 57 ||
  // Numeric keypad
      key >= 96 && key <= 105 ||
  // comma, period and minus, . on keypad
    key == 190 || key == 188 || key == 109 || key == 110 ||
  // Backspace and Tab and Enter
    key == 8 || key == 9 || key == 13 ||
  // Home and End
    key == 35 || key == 36 ||
  // left and right arrows
    key == 37 || key == 39 ||
  // Del and Ins
    key == 46 || key == 45 ||
  // Copy, Paste, cut and Select
    ((e.keyCode == 65 || e.keyCode == 86 || e.keyCode == 67 || e.keyCode == 88) && (e.ctrlKey === true || e.metaKey === true))) { return true }
  e.preventDefault()
  return false
}

// Font Awesome Loader spinner, an optional fontSize parameter
const renderSpinner = fontSize => {
  if (fontSize) { // if Font Size was sent
    return "<i style='font-size:" + fontSize + "px' class='fa fa-spinner fa-spin'></i>"
  }
  return "<i class='fa fa-spinner fa-spin'></i>"
}

// calculate size of file in KB's MB's GB's
const bytesToSize = bytes => {
  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
  if (bytes == 0) return '0 Byte'
  var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)))
  return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i]
}

// Return readable file name e.g. Image or PDF
const fileReadableType = fileName => {
  var fileExtention = fileName.substr(fileName.indexOf('.'))
  fileExtention = fileExtention.toLowerCase()
  if (fileExtention == ".jpg" || fileExtention == ".png" || fileExtention == ".tif" || fileExtention == ".jpeg") {
    return 'Image'
  } else if (fileExtention == ".xslx" || fileExtention == ".xls" || fileExtention == ".csv") {
    return 'Excel'
  } else if (fileExtention == ".doc" || fileExtention == ".docx") {
    return 'Word'
  } else if (fileExtention == ".pdf") {
    return 'PDF'
  }
  return 'Unknown'
}

// Convert currency to float value for DKK
const mySplice = value => {
  return value.toString().slice(0, -2) + "." + value.toString().slice(-2 + Math.abs(0))
}
// The number of rows to appear on a paginated table
const PAGINATED_ROWS = 10

const PAGINATED_FIVE = 5

// max file upload size
const MAX_FILE_SIZE = 8388608
// Sort Data by date
const sortByDate = object => {
  var data = object.data

  for (let i = 0; i < data.length; i++) {
    for (let j = 0; j < data.length; j++) {
      if (moment(String(data[i].created)).diff(moment(String(data[j].created))) < 0) {
        let temp = data[i]
        data[i] = data[j]
        data[j] = temp
      }
    }
  }
  object.data = data
  return object
}

// get parameter from URL
const getParameterByName = name => {
  let url = window.location.href
  name = name.replace(/[\[\]]/g, '\\$&')
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)')
  var results = regex.exec(url)
  if (!results) return null
  if (!results[2]) return ''
  return decodeURIComponent(results[2].replace(/\+/g, ' '))
}

// convert date for our date selector
const convertDate = d => {
  let date = d.toString()
  var parts = date.split(" ")
  var months = {
    Jan: "01",
    Feb: "02",
    Mar: "03",
    Apr: "04",
    May: "05",
    Jun: "06",
    Jul: "07",
    Aug: "08",
    Sep: "09",
    Oct: "10",
    Nov: "11",
    Dec: "12"
  }
  return parts[3] + "-" + months[parts[1]] + "-" + parts[2]
}

const notify = (text, time = 3000, type = "success", route = "") => {
  if (type == "success") {
    type = "is-primary-light"
  } else {
    type = "is-info"
  }
  if (route !== "") {
    text = '<a href="/' + route + '" >' + text + '</a >'
  }
  if (toast !== false) {
    toast.close()
  }
  toast = Toast.open({
    message: text,
    type: type,
    position: 'is-bottom',
    duration: time,
    queue: false
  })
}

const getStringNumbers = txt => {
  // remove all non difits from string and return int value
  try {
    var numb = txt.match(/\d/g)
    numb = numb.join("")
    return parseInt(numb)
  } catch (err) {

  }
}

export {
  countObjectProperties,
  validEmail,
  allowNumbers,
  renderSpinner,
  bytesToSize,
  fileReadableType,
  PAGINATED_FIVE,
  PAGINATED_ROWS,
  MAX_FILE_SIZE,
  mySplice,
  sortByDate,
  getParameterByName,
  convertDate,
  notify,
  getStringNumbers
}
