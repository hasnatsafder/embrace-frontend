'use strict'
module.exports = {
  NODE_ENV: '"production"',
  ROOT_API: '"https://admin.debito.dk/api/v1"',
  ROOT_ADDRESS: '"https://admin.debito.dk/"'
}
