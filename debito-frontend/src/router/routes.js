import MainLayout from '@/pages/_layouts/MainLayout'
import PageAuth from '@/pages/auth/PageAuth.vue'

import Dashboard from '@/pages/dashboard/Dashboard.vue'
import ClaimLandingPage from '@/pages/ClaimLandingPage.vue'

// debtors
import DebtorsCreate from '@/pages/debtors/DebtorsCreate.vue'
import DebtorsIndex from '@/pages/debtors/DebtorsIndex.vue'
import DebtorEdit from '@/pages/debtors/DebtorEdit.vue'

// accounts
import AccountsCreate from '@/pages/accounts/AccountsCreate.vue'
import AccountsIndex from '@/pages/accounts/AccountsIndex.vue'
import AccountEdit from '@/pages/accounts/AccountEdit.vue'
import AccountSelection from '@/pages/accounts/AccountSelection.vue'

// claims
import Claims from '@/pages/claims/ClaimsIndex.vue'
import ClaimsView from '@/pages/claims/ClaimsView.vue'
import ClaimsCreate from '@/pages/claims/ClaimsCreate.vue'

// user
import UserProfile from '@/pages/profile/UserProfile.vue'
import UserSettings from '@/pages/profile/UserSettings'
// erp integration
import AccountingSystemIndex from '@/pages/accountingSystem/AccountingSystemIndex.vue'
import BillyIntegration from '@/pages/accountingSystem/BillyIntegration.vue'
import DineroIntegration from '@/pages/accountingSystem/DineroIntegration.vue'
import ErpSyncing from '@/pages/accountingSystem/ErpSyncing.vue'

// dunning system
import dunning from '@/pages/dunningSystem/dunning.vue'

export const routes = [
  {
    path: '/login',
    name: 'login',
    component: PageAuth,
    props: { action: 'login' },
    meta: { requiresGuest: true }
  },
  {
    path: '/signup',
    name: 'signup',
    component: PageAuth,
    props: { action: 'signup' },
    meta: { requiresGuest: true }
  },
  {
    path: '/forgotpassword',
    name: 'forgotpassword',
    component: PageAuth,
    props: { action: 'forgotpassword' },
    meta: { requiresGuest: true }
  },
  {
    path: '/resetpassword',
    name: 'resetpassword',
    component: PageAuth,
    props: { action: 'resetpassword' },
    meta: { requiresGuest: true }
  },
  {
    path: '/createaccount',
    name: 'createaccount',
    component: PageAuth,
    props: { action: 'createaccount' },
    meta: { requiresAuth: true },
  },
  {
    path: '/404',
    component: PageAuth,
    props: { action: 'notfound' },
    meta: { requiresGuest: true }
  },
  { path: '*', redirect: '/404' },
  {
    path: '/',
    component: MainLayout,
    meta: { requiresAuth: true },
    children: [
      {
        path: '/',
        name: 'dashboard',
        component: Dashboard
      },
      {
        path: '/accounts',
        name: 'accounts-index',
        component: AccountsIndex
      },
      {
        path: '/accounts/create',
        name: 'accounts-create',
        component: AccountsCreate
      },
      {
        path: '/accounts/selection',
        name: 'account-selection',
        component: AccountSelection
      },
      {
        path: '/account/edit/:id',
        name: 'accounts-edit',
        component: AccountEdit,
        props: true
      },
      {
        path: '/debtors',
        name: 'debtors-index',
        component: DebtorsIndex
      },
      {
        path: '/usersettings',
        name: 'user-settings',
        component: UserSettings
      },
      {
        path: '/debtors/create/:accessType?',
        name: 'debtors-create',
        component: DebtorsCreate
      },
      {
        path: '/debtors/edit/:id',
        name: 'debtors-edit',
        component: DebtorEdit,
        props: true
      },
      {
        path: '/ClaimLandingPage',
        name: 'ClaimLandingPage',
        component: ClaimLandingPage
      },
      {
        path: '/claims',
        name: 'claims',
        component: Claims
      },
      {
        path: '/claims/view/:claim_id',
        name: 'claims-view',
        component: ClaimsView,
        props: true
      },
      {
        path: '/claims/create',
        name: 'claims-create',
        component: ClaimsCreate
      },
      {
        path: '/user/profile',
        name: 'user-profile',
        component: UserProfile
      },
      {
        path: '/accountingsystem',
        name: 'account-system',
        component: AccountingSystemIndex
      },
      {
        path: '/accountingsystem/billy',
        name: 'billy',
        component: BillyIntegration
      },
      {
        path: '/accountingsystem/dinero',
        name: 'dinero',
        component: DineroIntegration
      },
      {
        path: '/accountingsystem/syncing/:reference',
        name: 'erp_syncing',
        props: true,
        component: ErpSyncing
      },
      {
        path: '/accountingsystem/dunning/:invoice_id',
        name: 'dunning',
        props: true,
        component: dunning
      }
    ]
  }
]
