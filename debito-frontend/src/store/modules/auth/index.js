import actions from './actions.js'
import mutations from './mutations.js'
import { renderSpinner } from '@/helpers'

export default {
  namespaced: true,
  state: {
    formInput: {
      email: null,
      password: null,
      phone: null,
      rememberMe: null,
      error: false,
      serverError: false,
      buttonDisabled: false,
      serverSuccess: false,
      token_verified: null,
      token: "",
      first_name: null,
      last_name: null,
      infoButtonDisabled: false
    },
    styleObject: {
      errorObject: {
        email: null,
        password: null,
        phone: null
      }
    },
    userInfoLoading: false
  },
  getters: {
    buttonSpinner: (state, fontSize) => {
      return renderSpinner(fontSize)
    }
  },
  actions,
  mutations,
}
