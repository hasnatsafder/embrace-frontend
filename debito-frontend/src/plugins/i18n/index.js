import Vue from 'vue';
import VueI18n from 'vue-i18n';
 
const data = require('./message.json');
  
Vue.use(VueI18n)
 
const i18n = new VueI18n({
  locale: navigator.language.split("-")[0],   // Use browser first language
  fallbackLocale: 'en',
  messages: {
    'en': require('./locales/en.json'),
    'da': require('./locales/da.json')
  },
  silentTranslationWarn: false
});
export default i18n