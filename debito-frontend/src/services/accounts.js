import Vue from 'vue'
export default {

  // get users account
  getAccounts () {
    return Vue.axios.get('/accounts?limit=300&showCountries=1')
      .then(response => {
        return response.data
      })
  },

  // create user accounts
  createAccount (postObj) {
    return Vue.axios.post('/accounts', postObj)
      .then(response => {
        return response.data
      })
      .catch(error => {
        return error.data
      })
  },

  // fetch all accounts in accounts index table
  fetchInfo(fetch_info) {
    return Vue.axios.get('/accounts/' + fetch_info + "?showCountries=1")
      .then(response => {
        return response
      })
  },

  // edit Accounts in Accounts section
  editAccount(edit_id, state_postObj) {
    return Vue.axios.put('/accounts/' + edit_id, state_postObj)
      .then(response => {
        return response
      })
      .catch(error => {
        return error
      })
  },

  // fetching countries info
  getCountries() {
    return Vue.axios.get('/countries?limit=300')
      .then(response => {
        return response
      })
  }
}
