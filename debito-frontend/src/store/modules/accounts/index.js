import actions from './actions.js'
import mutations from './mutations.js'

export default {
  namespaced: true,
  state: {
    accounts: [],
    countries: [],
    userProfile: null,
    formInput: {
      notification_summary_interval: 'DAILY',
      created_by_id: null,
      company_type_id: 1,
      cvr: null,
      company_name: null,
      address: null,
      zip_code: null,
      city: null,
      telephone: null,
      vat_number: null,
      website: null,
      email: null,
      is_company: 1,
      bank_reg_number: null,
      bank_account_number: null,
      ean_number: null,
      country: null,
      country_name: null,
      country_iso_code: null,
      error: false, // for front end validations errors
      serverError: false,
      disabled: false,
      dunning_configuration_id: null,
    },
    styleObject: {
      errorObject: {
        vat: null,
        company_name: null,
        address: null,
        zip_code: null,
        city: null,
        telephone: null,
        website: null,
        email: null,
        ean: null,
        reg_num: null,
        account_num: null,
        country: null,
        iban: null,
        swift_code: null,
      }
    },
    postObject: null,
    routerPath: null,
    id: null, // only in case of edit form,
    tableLoading: false,
    fieldLimits: {
      vat_number: 8,
      zip_code: 4,
      phone: 8
    }
  },
  getters: {
    activeAccountName: function(state) {
      if (state.userProfile && state.accounts.data) {
        var data = state.accounts.data
        var active_account_detail = Object.values(data)
          .filter(account => account.id === state.userProfile[0].active_account_id)
        if (typeof active_account_detail !== 'undefined' && active_account_detail.length > 0) {
          return active_account_detail[0].name
        }
      }
      return ""
    },
    companyButton: (state) => {
      if (state.formInput.is_company == 1) {
        return "active"
      } else {
        return ""
      }
    },
    privateButton: (state) => {
      if (state.formInput.is_company == 0) {
        return "active"
      } else {
        return ""
      }
    },
    activeAccountInfo: function(state) {
      if (state.userProfile && state.accounts.data) {
        var data = state.accounts.data
        var active_account_detail = Object.values(data)
          .filter(account => account.id === state.userProfile[0].active_account_id)
        if (typeof active_account_detail !== 'undefined' && active_account_detail.length > 0) {
          return active_account_detail[0]
        }
      }
      return ""
    },
  },
  actions,
  mutations,
}
