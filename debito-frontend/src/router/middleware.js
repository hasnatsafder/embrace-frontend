import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Router from './'

Vue.use(VueAxios, axios)

axios.interceptors.request.use((request) => {
  if (request.url[0] === '/') {
    request.headers = { "Content-Type": "application/json", "Accept": "application/json", "Access-Control-Allow-Origin": '*' }
    request.url = process.env.ROOT_API + request.url // ROOT_API defined in config env files (selects for dev and production)
    request.crossDomain = true
    const token = Vue.auth.getToken()
    if (token) {
      request.headers['Authorization'] = "Bearer " + token
    }
  }
  return request
})

Router.beforeEach(function (to, from, next) {
  if (to.matched.some(function (record) { return record.meta.requiresGuest }) && Vue.auth.loggedIn()) {
    next({
      path: '/'
    })
  } else if (to.matched.some(function (record) { return record.meta.requiresAuth }) && !Vue.auth.loggedIn()) {
    next({
      path: '/login',
      query: { redirect: to.fullPath }
    })
  } else {
    next()
  }
})
