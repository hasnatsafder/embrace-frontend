export default {
  setEmailError(state) {
    state.styleObject.errorObject.email = "is-danger"
    state.formInput.error = true
  },
  setPasswordError(state) {
    state.styleObject.errorObject.password = "is-danger"
    state.formInput.error = true
  },
  setPhoneError(state) {
    state.styleObject.errorObject.phone = "is-danger"
    state.formInput.error = true
  },
  resetSetError(state) {
    state.styleObject.errorObject.email = ""
    state.styleObject.errorObject.password = ""
    state.formInput.error = false
    state.formInput.serverError = false
    state.formInput.serverSuccess = false
  },
  setSeverError(state, val) {
    state.formInput.serverError = val
  },
  setSeverSuccess(state, val) {
    state.formInput.serverSuccess = val
  },
  setButtonDisabled(state, val) {
    state.formInput.buttonDisabled = val
  },
  setTokenVerified(state, val) {
    state.formInput.token_verified = val
  },
  resetFormInputs(state) {
    state.formInput.email = state.formInput.password = state.formInput.rememberMe = null
    state.formInput.error = state.formInput.serverError = state.formInput.disabled = false
    state.formInput.buttonDisabled = false
  },
  setTokenValue(state, val) {
    state.formInput.token = val
  },
  resetPasswordAndEmail(state) {
    state.formInput.email = ""
    state.formInput.password = ""
  },
  setUserInfo(state, val) {
    state.formInput.first_name = val.first_name
    state.formInput.last_name = val.last_name
    state.formInput.phone = val.phone
    state.formInput.email = val.email
  },
  resetUserSettingForm(state) {
    state.formInput.buttonDisabled = false
    state.formInput.infoButtonDisabled = false
    state.formInput.error = ''
    state.formInput.serverError = ''
  }

}
