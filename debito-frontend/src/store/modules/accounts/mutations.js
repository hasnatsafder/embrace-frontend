export default {
  resetSetError(state) {
    state.styleObject.errorObject.vat = null
    state.styleObject.errorObject.company_name = null
    state.styleObject.errorObject.first_last_name = null
    state.styleObject.errorObject.address = null
    state.styleObject.errorObject.zip_code = null
    state.styleObject.errorObject.city = null
    state.styleObject.errorObject.telephone = null
    state.styleObject.errorObject.email = null
    state.styleObject.errorObject.reg_num = null
    state.styleObject.errorObject.account_num = null
    state.styleObject.errorObject.country = null
    state.styleObject.errorObject.iban = null
    state.styleObject.errorObject.swift_code = null
    state.formInput.error = false
    state.formInput.serverError = false
    this.setRouterHome = ""
  },
  setVatError(state) {
    state.styleObject.errorObject.vat = "has-error"
    state.formInput.error = true
  },
  setCompanyNameError(state) {
    state.styleObject.errorObject.company_name = "has-error"
    state.formInput.error = true
  },
  setAddressError(state) {
    state.styleObject.errorObject.address = "has-error"
    state.formInput.error = true
  },
  setZipcodeError(state) {
    state.styleObject.errorObject.zip_code = "has-error"
    state.formInput.error = true
  },
  setCityError(state) {
    state.styleObject.errorObject.city = "has-error"
    state.formInput.error = true
  },
  setTelephoneError(state) {
    state.styleObject.errorObject.telephone = "has-error"
    state.formInput.error = true
  },
  setEmailError(state) {
    state.styleObject.errorObject.email = "has-error"
    state.formInput.error = true
  },
  setRegisterationNumberError(state) {
    state.styleObject.errorObject.reg_num = "has-error"
    state.formInput.error = true
  },
  setAccountNumberError(state) {
    state.styleObject.errorObject.account_num = "has-error"
    state.formInput.error = true
  },
  setCountryError(state) {
    state.styleObject.errorObject.country = "has-error"
    state.formInput.error = true
  },
  setIbanError(state) {
    state.styleObject.errorObject.iban = "has-error"
    state.formInput.error = true
  },
  setSwiftcodeError(state) {
    state.styleObject.errorObject.swift_code = "has-error"
    state.formInput.error = true
  },
  setButtonDisabled(state, val) {
    state.formInput.disabled = val
  },
  setRouterHome(state) {
    state.routerPath = "/accounts"
  },
  setSeverError(state, val) {
    state.formInput.serverError = val
  },
  setAccountNumberLength(state) {
    // if length of account less than 10, then append zeros at start
    var len = 10 - state.formInput.bank_account_number.length
    var str = new Array(len + 1).join('0')
    state.formInput.bank_account_number = str + state.formInput.bank_account_number
  },
  setEditState(state, data) {
    state.formInput.vat_number = data.vat_number
    state.formInput.company_name = data.name
    state.formInput.address = data.address
    state.formInput.zip_code = data.zip_code
    state.formInput.city = data.city
    state.formInput.telephone = data.phone
    state.formInput.email = data.email
    state.formInput.website = data.website
    state.formInput.ean_number = data.ean_number
    state.formInput.bank_reg_number = data.bank_reg_number
    state.formInput.bank_account_number = data.bank_account_number
    state.formInput.iban = data.iban
    state.formInput.swift_code = data.swift_code
    state.formInput.country = data.country_id
    state.formInput.country_name = data.Countries.name
    state.formInput.country_iso_code = data.Countries.iso_code
    state.formInput.is_company = data.is_company ? 1 : 0 // we recieve value from sever in true and false
  },
  setStateId(state, id) {
    state.id = id
  },
  resetformState(state) {
    state.formInput.vat_number = null
    state.formInput.company_name = null
    state.formInput.address = null
    state.formInput.zip_code = null
    state.formInput.city = null
    state.formInput.telephone = null
    state.formInput.email = null
    state.formInput.website = null
    state.formInput.ean_number = null
    state.formInput.bank_reg_number = null
    state.formInput.bank_account_number = null
    state.formInput.iban = null
    state.formInput.swift_code = null
    state.formInput.country = null
    state.formInput.country_name = null
    state.formInput.country_iso_code = null
    state.formInput.is_company = 1
    state.formInput.company_type_id = 1
    state.formInput.error = false
    state.formInput.serverError = false
    state.formInput.disabled = false
  },
  resetAccounts(state) {
    state.accounts = []
    state.userProfile = null
  },
  setAccounts(state, val) {
    let accountsData = Object.values(val.data)
    accountsData.forEach(account => {
      account.accountType = account.is_company ? 'Company' : 'Private'
    })
    state.accounts = val
    state.accounts.data = accountsData
  },
  setTableLoading(state, val) {
    state.tableLoading = val
  },
  setCountries(state, val) {
    state.countries = val
  },
  setFieldLimits(state) {
    if (state.formInput.country_iso_code && state.formInput.country_iso_code != "DK") {
      state.fieldLimits.vat_number = 20
      state.fieldLimits.zip_code = 20
      state.fieldLimits.phone = 20
    } else {
      state.fieldLimits.vat_number = 8
      state.fieldLimits.zip_code = 4
      state.fieldLimits.phone = 8
    }
  },
  setCvrsearchResult(state, data) {
    state.formInput.vat_number = data.vat
    state.formInput.company_name = data.name
    state.formInput.address = data.address
    state.formInput.zip_code = data.zipcode
    state.formInput.telephone = data.phone
    state.formInput.city = data.city
    state.formInput.email = data.email
  },
  resetCvrsearchResult(state, data) {
    if (data === 'name') {
      state.formInput.vat_number = null
    } else {
      state.formInput.company_name = null
    }
    state.formInput.address = null
    state.formInput.zip_code = null
    state.formInput.telephone = null
    state.formInput.city = null
    state.formInput.email = null
  },
  setDunningConfigrations(state, data) {

  }
}
