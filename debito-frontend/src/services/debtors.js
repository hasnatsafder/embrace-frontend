import Vue from 'vue'

export default {

  // get debtors
  getDebtors(pageNo, searchValue) {
    return Vue.axios.get(`/debtors/myDebtors?${pageNo}${searchValue}`)
      .then(response => {
        return response
      })
  },

}
