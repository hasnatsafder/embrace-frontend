import actions from './actions.js'
import mutations from './mutations.js'
// i18n.locale = "da"

export default {
  namespaced: true,
  state: {
    debtors: [],
    formInput: {
      id: 0,
      created_by_id: null,
      company_type_id: 1,
      company_name: null,
      address: null,
      zip_code: null,
      city: null,
      phone: null,
      vat_number: null,
      is_company: 1,
      first_name: null,
      last_name: null,
      email: null,
      country: null,
      country_name: null,
      country_iso_code: null,
      account_id: null,
      error: false, // for front end validations errors
      serverError: false,
      disabled: false
    },
    styleObject: {
      errorObject: {
        vat: null,
        company_name: null,
        first_name: null,
        last_name: null,
        address: null,
        zip_code: null,
        city: null,
        phone: null,
        email: null,
        country: null,
      }
    },
    postObject: null,
    routerPath: null,
    id: null, // only in case of edit form,
    tableLoading: false,
    fieldLimits: {
      vat_number: 8,
      zip_code: 4,
      phone: 8
    }
  },
  getters: {
    companyButton: (state) => {
      if (state.formInput.is_company == 1) {
        return "active"
      } else {
        return ""
      }
    },
    privateButton: (state) => {
      if (state.formInput.is_company == 0) {
        return "active"
      } else {
        return ""
      }
    }
  },
  actions,
  mutations,
}
