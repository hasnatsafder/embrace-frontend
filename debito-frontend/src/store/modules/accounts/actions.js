import i18n from '@/plugins/i18n'
import router from '@/router/index'
import store from '@/store/index'
import { validEmail, notify } from '@/helpers'
import { SnackbarProgrammatic as Snackbar } from 'buefy'
import accountService from '@/services/accounts'
import userService from '@/services/users'
import Vue from "vue"

export default {
  deleteAccount({ dispatch }, id) {
    Vue.axios({
      method: "delete",
      url: `/accounts/${id}`,
      headers: {
        "Content-Type": "application/json"
      }
    }).then(response => {
      if (response.data.success) {
        Snackbar.open({
          
          duration: 10000,
          message: i18n.t('account.account_deleted'),
          type: 'is-warning',
          position: 'is-bottom',
          actionText: 'Undo',
          queue: false,
          onAction: () => {
            dispatch('undoAccount', id)
          }
        })
      } else {
        Snackbar.open({
          duration: 10000,
          message: i18n.t('account.cannot_delete'),
          type: 'is-warning',
          actionText: 'Okay',
          position: 'is-bottom',
          queue: false,
        })
      }
      dispatch('getAccounts')
    })
  },
  undoAccount({ dispatch }, id) {
    Vue.axios.get('/accounts/restore/' + id).then(response => {
      dispatch('getAccounts')
    })
  },
  // get total accounts related to a user
  getAccounts ({ state, commit }) {
    accountService.getAccounts().then(
      response => {
        commit('setAccounts', response)
      }
    )
  },
  // get the details about the user profile
  getUserProfile({ state }) {
    if (localStorage.getItem('authToken') !== null) {
      userService.getUserProfile().then(response => {
        state.userProfile = response.data.data
      })
    }
  },
  // to choose an account
  setActiveAccount({ state, commit, dispatch }, account_id) {
    commit("setTableLoading", true)
    userService.setActiveAccount(account_id, state.userProfile[0].id)
      .then(response => {
        dispatch("getUserProfile") // --NOTE--: will be replaced by response data, once PUT Redirection fixed on localhost
        store.dispatch("accountingSystem/integrationCheck")
        store.dispatch("debtors/updateDebtorsState")
        store.dispatch("claims/getClaims")
        commit("setTableLoading", false)
      })
  },
  // actions for creating account
  createAccount({ state, commit, dispatch }, routerLink) {
    commit("resetSetError")
    if (state.formInput.country_iso_code == "DK") {
      dispatch("accountFormValidationsDenmark")
    } else {
      dispatch("accountFormValidationsInternational")
    }
    if (state.formInput.error === false) {
      commit("setButtonDisabled", true)
      dispatch("makePostObject")
      accountService.createAccount(state.postObject).then(response => {
        router.push(routerLink)
        store.dispatch("debtors/updateDebtorsState")
        store.dispatch("claims/getClaims")
        dispatch("updateAccountsState")
        notify(i18n.t('account.toastr_account'))
      })
        .catch(error => {
          commit("setButtonDisabled", false)
          dispatch("showSignupError", error.response.data.data.errors)
        })
    }
  },
  // validations for input types in create /edit account form
  accountFormValidationsDenmark({ state, commit }) {
    if (state.formInput.is_company === 1 && (state.formInput.vat_number === null || (state.formInput.vat_number.toString()).length !== 8 || (state.formInput.vat_number.toString()).match(/^[0-9]+$/) == null)) { // not in case of private
      commit("setVatError")
    }
    if (state.formInput.company_name === "" || state.formInput.company_name === null) {
      commit("setCompanyNameError")
    }
    if (state.formInput.address === "" || state.formInput.address === null) {
      commit("setAddressError")
    }
    if (state.formInput.country === "" || state.formInput.country === null) {
      commit("setCountryError")
    }
    if (state.formInput.zip_code === null || state.formInput.zip_code.length !== 4 || state.formInput.zip_code.match(/^[0-9]+$/) == null) {
      commit("setZipcodeError")
    }
    if (state.formInput.city === "" || state.formInput.city === null) {
      commit("setCityError")
    }
    if (state.formInput.telephone == null || state.formInput.telephone.length !== 8 || state.formInput.telephone == "") {
      commit("setTelephoneError")
    }
    if (state.formInput.bank_reg_number === null || state.formInput.bank_reg_number.length != 4 || state.formInput.bank_reg_number.match(/^[0-9]+$/) == null) {
      commit("setRegisterationNumberError")
    }
    if (state.formInput.bank_account_number === "" || state.formInput.bank_account_number === null || state.formInput.bank_account_number.length > 10 || state.formInput.bank_account_number.match(/^[0-9]+$/) == null) {
      commit("setAccountNumberError")
    }
    if (state.formInput.email !== null && state.formInput.email !== "") {
      if (!validEmail(state.formInput.email)) {
        commit('setEmailError')
      }
    }
    if (state.formInput.email == null || state.formInput.email == "") {
      commit('setEmailError')
    }
  },
  accountFormValidationsInternational({ state, commit }) {
    if (state.formInput.is_company === 1 && (state.formInput.vat_number === null || (state.formInput.vat_number.toString()).match(/^[0-9]+$/) == null)) { // not in case of private
      commit("setVatError")
    }
    if (state.formInput.company_name === "" || state.formInput.company_name === null) {
      commit("setCompanyNameError")
    }
    if (state.formInput.address === "" || state.formInput.address === null) {
      commit("setAddressError")
    }
    if (state.formInput.country === "" || state.formInput.country === null) {
      commit("setCountryError")
    }
    if (state.formInput.zip_code === null || state.formInput.zip_code.match(/^[0-9]+$/) == null) {
      commit("setZipcodeError")
    }
    if (state.formInput.city === "" || state.formInput.city === null) {
      commit("setCityError")
    }
    if (state.formInput.telephone == null || state.formInput.telephone.match(/^[0-9]+$/) == null) {
      commit("setTelephoneError")
    }
    if (state.formInput.iban === "" || state.formInput.iban === null || state.formInput.iban.match(/^[0-9]+$/) == null) {
      commit("setIbanError")
    }
    if (state.formInput.swift_code === "" || state.formInput.swift_code === null) {
      commit("setSwiftcodeError")
    }
    if (state.formInput.email !== null && state.formInput.email !== "") {
      if (!validEmail(state.formInput.email)) {
        commit('setEmailError')
      }
    }
    if (state.formInput.email == null || state.formInput.email == "") {
      commit('setEmailError')
    }
  },
  makePostObject({ state, commit }) {
    // prepare account number, if less than 10 than append zeroes
    if (state.formInput.bank_account_number && state.formInput.bank_account_number.length < 10) {
      commit("setAccountNumberLength")
    }
    state.postObject = {
      name: state.formInput.company_name,
      address: state.formInput.address,
      zip_code: state.formInput.zip_code,
      country_id: state.formInput.country,
      city: state.formInput.city,
      phone: state.formInput.telephone,
      email: state.formInput.email,
      vat_number: state.formInput.vat_number,
      ean_number: state.formInput.ean_number,
      website: state.formInput.website,
      bank_reg_number: state.formInput.bank_reg_number,
      bank_account_number: state.formInput.bank_account_number,
      iban: state.formInput.iban,
      swift_code: state.formInput.swift_code,
      is_company: state.formInput.is_company,
    }
  },
  showSignupError({ state, commit }, errorObject) {
    return commit('setSeverError', i18n.t("auth.something_went_wrong"))
  },

  // fetch account index table
  fetchInfo({ state, commit }) {
    commit("setTableLoading", true)
    accountService.fetchInfo(state.id).then(response => {
      commit("setEditState", response.data.data)
      commit("setTableLoading", false)
      commit('setFieldLimits')
    })
  },
  // Editing account from accounts section
  editAccount({ state, commit, dispatch }) {
    commit('resetSetError')
    if (state.formInput.country_iso_code == "DK") {
      dispatch("accountFormValidationsDenmark")
    } else {
      dispatch("accountFormValidationsInternational")
    }
    if (state.formInput.error === false) {
      commit('setButtonDisabled', true)
      dispatch('makePostObject')
      accountService.editAccount(state.id, state.postObject)
        .then(response => {
          router.push('/accounts')
          dispatch('updateAccountsState')
          notify(i18n.t('account.toastr_account_edit'))
        })
        .catch(error => {
          commit('setButtonDisabled', false)
          dispatch('showSignupError', error.response.data.data.errors)
        })
    }
  },
  // fetch country info
  getCountries({ state, commit }) {
    if (state.countries.length != 0) {
      return
    }
    accountService.getCountries().then(response => {
      commit('setCountries', response.data.data)
    })
  },
  updateAccountsState({ dispatch, commit }) {
    commit("resetAccounts")
    dispatch("getAccounts")
    dispatch("getUserProfile")
  },
  getDunningConfigurations({ state, commit }) {
    return Vue.axios
      .get("/dunning-configurations/")
  },
  postDunningConfigurations({ state }, data) {
    if (data.method == "post") {
      return Vue.axios.post('/dunning-configurations/', data.data)
    }
    else {
      return Vue.axios.put('/dunning-configurations/' + data.id, data.data)
    }
  }
}
