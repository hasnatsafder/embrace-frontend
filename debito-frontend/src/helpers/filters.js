import Vue from "vue"
import moment from 'moment'
import numeral from 'numeral'
import { mySplice, getStringNumbers } from '@/helpers'
import numeralen from "numeral/locales/da-dk"

Vue.filter("formatCurrency", value => {
  numeral.locale('da-dk')
  if (!value) {
    return "-"
  }
  let number_original = mySplice(value)
  number_original = parseFloat(number_original)
  return numeral(number_original).format("0,0.00")
})

Vue.filter("formatDate", value => {
  if (value) {
    return moment(String(value)).format('DD-MM-YYYY')
  }
  return "-"
})

Vue.filter("formatDateTime", value => {
  if (value) {
    return moment(String(value)).format('DD-MM-YYYY HH:mm')
  }
  return "-"
})

Vue.filter('setDebtorName', value => {
  if (value.company_name) {
    return value.company_name
  } else if (value.first_name && value.last_name) {
    return value.first_name + " " + value.last_name
  } else {
    return value.first_name
  }
})
// Separate filter modified for invoice amount testing to avoid issues in other tables
Vue.filter("formatInvoiceAmount", value => {
  value = value.toString()
  if (value.includes(",")) {
    if (value.split(",")[1].length === 1) {
      value = getStringNumbers(value)
      value = value * 10
    } else if (value.split(",")[1].length > 1) {
      value = getStringNumbers(value)
      value = value * 1
    }
  } else if (value.includes(".")) {
    if (value.split(".")[1].length === 1) {
      value = getStringNumbers(value)
      value = value * 10
    } else if (value.split(".")[1].length > 1) {
      value = getStringNumbers(value)
      value = value * 100
    }
  } else {
    value = value * 100
  }
  numeral.locale('da-dk')
  if (!value) {
    return ""
  }
  let number_original = mySplice(value)
  number_original = parseFloat(number_original)
  return numeral(number_original).format("0,0.00")
})

Vue.filter("formatCurrencyTextBox", value => {
  value = value.toString()
  if (value.includes(",")) {
    if (value.split(",")[1].length === 1) {
      value = getStringNumbers(value)
      value = value * 10
    } else if (value.split(",")[1].length > 1) {
      return value
    }
  } else {
    value = value * 100
  }
  numeral.locale('da-dk')
  if (!value) {
    return ""
  }
  let number_original = mySplice(value)
  number_original = parseFloat(number_original)
  return numeral(number_original).format("0,0.00")
})

Vue.filter('ifEmptyDash', string => {
  if (string === "" || string === null) {
    return "-"
  }
  return string
})

Vue.filter('subStr', string => {
  if (string.length > 23) {
    var afterDot = string.substr(string.indexOf("..."))
    return string.substring(0, 19) + "... " + afterDot
  }
  return string
})
Vue.filter('subStrParam', function(string, number) {
  let filterLength = number - 4
  if (string.length > number) {
    var afterDot = string.substr(string.indexOf("..."))
    return string.substring(0, filterLength) + "... " + afterDot
  }
  return string
})

Vue.filter('toCapitalLetters', string => {
  return string.toUpperCase()
})

Vue.filter('formatDecimal', value => {
  numeral.locale('da-dk')
  return (String(value)).replace('.', ',')
})

Vue.filter("splitAfterdash", value => {
  let val = value.split("-")
  return val[1]
})
