import Vue from 'vue'
import Layout from './layout/index.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.config.productionTip = false
new Vue({
  router,
  store,
  vuetify,
  render: h => h(Layout)
}).$mount('#app')