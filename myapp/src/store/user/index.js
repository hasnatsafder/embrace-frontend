import Getters from './getter'

export default {
    namespaced: true,
    Getters
}