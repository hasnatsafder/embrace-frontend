export default {
  setAnimatedLoader(state, val) {
    state.animatedLoader = val
  },
  setInvoiceDunnings(state, val) {
    state.invoiceDunnings = val
  },
  resetInvoiceDunnings(state) {
    state.invoiceDunnings = []
  },
  setApiError(state) {
    state.styleObject.errorObject.api_key = "has-error"
    state.formInput.error = true
  },
  setErrorMessage(state, val) {
    state.errorMessage = val
  },
  setDeleted(state, val) {
    state.deleted = val
  },
  setLoading (state, val) {
    state.loading = val
  },
  setShowInvoices(state, val) {
    state.showInvoices = val
  },
  setEmailError(state) {
    state.styleObject.errorObject.email = "has-error"
    state.formInput.error = true
  },
  setPasswordError(state) {
    state.styleObject.errorObject.password = "has-error"
    state.formInput.error = true
  },
  resetSetError(state) {
    state.styleObject.errorObject.email = ""
    state.styleObject.errorObject.password = ""
    state.styleObject.errorObject.api_key = ""
    state.formInput.error = false
    state.formInput.serverError = false
  },
  setSeverError(state, val) {
    state.formInput.serverError = val
  },
  setButtonDisabled(state, val) {
    state.formInput.buttonDisabled = val
  },
  setVendor(state, val) {
    state.vendor = val
  },
  resetVendor(state) {
    state.vendor = null
  },
  setInvoices(state, data) {
    state.invoices = data
    let invoicesData = Object.values(data.data)
    invoicesData.forEach(invoice => {
      // set invoice values to match formatting in DK
      if (invoice.gross_amount % 1 != 0) { // if number has decimals
        let decimals = invoice.gross_amount.toString().split(".")[1].length
        if (decimals == 1) {
          invoice.gross_amount += "0"
        }
      } else {
        invoice.gross_amount += ".00"
      }
      if (invoice.net_amount % 1 != 0) { // if number has decimals
        let decimals = invoice.net_amount.toString().split(".")[1].length
        if (decimals == 1) {
          invoice.net_amount += "0"
        }
      } else {
        invoice.net_amount += ".00"
      }

      // remove point
      invoice.gross_amount = invoice.gross_amount.toString().replace(/\./g, "")
      invoice.net_amount = invoice.net_amount.toString().replace(/\./g, "")

      if (invoice.Debtors.company_name) {
        invoice.full_name = invoice.Debtors.company_name
      } else if (invoice.Debtors.first_name && invoice.Debtors.last_name) {
        invoice.full_name = invoice.Debtors.first_name + " " + invoice.Debtors.last_name
      } else {
        invoice.full_name = invoice.Debtors.first_name
      }
    })

    state.invoices = data
    state.invoices.data = invoicesData
  },
  resetInvoices(state) {
    state.invoices = []
  },
  setSelectedInvoices(state, val) {
    state.selectedInvoices = val
  },
  resetInvoicesPage(state) {
    state.selectedInvoices = []
    state.invoices = []
    state.formInput.api_key = null
    state.formInput.email = null
    state.formInput.password = null
    state.formInput.serverError = false
    state.formInput.buttonDisabled = false
    state.formInput.error = false
    state.formInput.organizations = []
    state.formInput.selectedOrganization = ""
  },
  setOrganization(state, val) {
    state.formInput.organizations = val
  },
  setSelectedOrganization(state, val) {
    state.formInput.selectedOrganization = String(val)
  },
  setSingleInvoice(state, data) {
    state.singleInvoice = data
  },
  setsyncing(state, val) {
    state.syncing = val
  },
  setStatsData(state, data) {
    state.statsData = Object.values(data.data.invoices)
  },
  resetStatsData(state) {
    state.statsData = {}
  },
  setAccountingSystemConnected(state, val) {
    state.accountingSystemConnected = val
  }
}
