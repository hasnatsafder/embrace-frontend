export default {
  resetSetError(state) {
    state.styleObject.errorObject.vat = null
    state.styleObject.errorObject.company_name = null
    state.styleObject.errorObject.first_name = null
    state.styleObject.errorObject.last_name = null
    state.styleObject.errorObject.address = null
    state.styleObject.errorObject.zip_code = null
    state.styleObject.errorObject.city = null
    state.styleObject.errorObject.phone = null
    state.styleObject.errorObject.email = null
    state.styleObject.errorObject.country = null
    state.formInput.error = false
    state.formInput.serverError = false
  },
  setVatError(state) {
    state.styleObject.errorObject.vat = "has-error"
    state.formInput.error = true
  },
  setCompanyNameError(state) {
    state.styleObject.errorObject.company_name = "has-error"
    state.formInput.error = true
  },
  setFirstNameError(state) {
    state.styleObject.errorObject.first_name = "has-error"
    state.formInput.error = true
  },
  setLastNameError(state) {
    state.styleObject.errorObject.last_name = "has-error"
    state.formInput.error = true
  },
  setAddressError(state) {
    state.styleObject.errorObject.address = "has-error"
    state.formInput.error = true
  },
  setZipcodeError(state) {
    state.styleObject.errorObject.zip_code = "has-error"
    state.formInput.error = true
  },
  setCityError(state) {
    state.styleObject.errorObject.city = "has-error"
    state.formInput.error = true
  },
  setPhoneError(state) {
    state.styleObject.errorObject.phone = "has-error"
    state.formInput.error = true
  },
  setEmailError(state) {
    state.styleObject.errorObject.email = "has-error"
    state.formInput.error = true
  },
  setAccountError(state) {
    state.styleObject.errorObject.account_id = "has-error"
    state.formInput.error = true
  },
  setButtonDisabled(state, val) {
    state.formInput.disabled = val
  },
  resetformState(state) {
    state.formInput.vat_number = null
    state.formInput.account_id = null
    state.formInput.address = null
    state.formInput.zip_code = null
    state.formInput.city = null
    state.formInput.phone = null
    state.formInput.is_company = 1
    state.formInput.company_name = null
    state.formInput.first_name = null
    state.formInput.last_name = null
    state.formInput.email = null
    state.formInput.country = null
    state.formInput.country_name = null
    state.formInput.country_iso_code = null
    state.formInput.error = false
    state.formInput.serverError = false
    state.formInput.disabled = false
  },
  resetDebtors(state) {
    state.debtors = []
  },
  setStateId(state, id) {
    state.id = id
  },
  setEditState(state, data) {
    state.formInput.vat_number = data.vat_number
    state.formInput.company_name = data.company_name
    state.formInput.address = data.address
    state.formInput.zip_code = data.zip_code
    state.formInput.city = data.city
    state.formInput.phone = data.phone
    state.formInput.first_name = data.first_name
    state.formInput.last_name = data.last_name
    state.formInput.email = data.email
    state.formInput.account_id = data.account_id
    state.formInput.country = data.country_id
    state.formInput.country_name = data.Countries.name
    state.formInput.country_iso_code = data.Countries.iso_code
    state.formInput.is_company = data.is_company ? 1 : 0 // we recieve value from sever in true and false
  },
  setSeverError(state, val) {
    state.formInput.serverError = val
  },
  setFormInputId(state, data) {
    state.formInput.id = 45
  },
  setFieldLimits(state) {
    if (state.formInput.country_iso_code && state.formInput.country_iso_code != "DK") {
      state.fieldLimits.vat_number = 20
      state.fieldLimits.zip_code = 20
      state.fieldLimits.phone = 20
    } else {
      state.fieldLimits.vat_number = 8
      state.fieldLimits.zip_code = 4
      state.fieldLimits.phone = 8
    }
  },
  setTableLoading(state, val) {
    state.tableLoading = val
  },
  setCvrsearchResult(state, data) {
    state.formInput.vat_number = data.vat
    state.formInput.company_name = data.name
    state.formInput.address = data.address
    state.formInput.zip_code = data.zipcode
    state.formInput.phone = data.phone
    state.formInput.city = data.city
    state.formInput.email = data.email
  },
  resetCvrsearchResult(state, data) {
    if (data === 'name') {
      state.formInput.vat_number = null
    } else {
      state.formInput.company_name = null
    }
    state.formInput.address = null
    state.formInput.zip_code = null
    state.formInput.telephone = null
    state.formInput.city = null
    state.formInput.email = null
  },
  setDebtors(state, data) {
    let debtorData = Object.values(data.data)
    debtorData.forEach(debtor => {
      if (debtor.company_name) {
        debtor.full_name = debtor.company_name
      } else if (debtor.first_name && debtor.last_name) {
        debtor.full_name = debtor.first_name + " " + debtor.last_name
      } else {
        debtor.full_name = debtor.first_name
      }
    })

    state.debtors = data
    state.debtors.data = debtorData
  },
}
