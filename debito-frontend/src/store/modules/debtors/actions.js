import i18n from '@/plugins/i18n'
import Vue from 'vue'
import { validEmail, notify } from '@/helpers'
import debtorService from '@/services/debtors'
import { SnackbarProgrammatic as Snackbar } from 'buefy'

export default {
  deleteDebtor({ dispatch }, id) {
    Vue.axios({
      method: "delete",
      url: `/debtors/${id}`,
      headers: {
        "Content-Type": "application/json"
      }
    }).then(response => {
      // notify(response.data.data, 5000)
      if (response.data.success == true) {
        Snackbar.open({
          duration: 10000,
          message: i18n.t('debtor.snack_deleted'),
          type: 'is-warning',
          position: 'is-bottom',
          actionText: 'Undo',
          queue: false,
          onAction: () => {
            dispatch('undoDebtor', id)
          }
        })
      } else {
        Snackbar.open({
          duration: 10000,
          message: i18n.t('debtor.cannot_delete'),
          type: 'is-warning',
          actionText: 'Okay',
          position: 'is-bottom',
          queue: false,
        })
      }
      console.log(response.data.success)
      dispatch('getDebtors')
    }).catch(error => {
      console.log(error)
    })
  },
  undoDebtor({ dispatch }, id) {
    Vue.axios.get('/debtors/restore/' + id).then(response => {
      dispatch('getDebtors')
    })
  },
  // to set debtors
  getDebtors ({ state, commit }, pageNo, searchValue) {
    commit('setTableLoading', true)
    pageNo = pageNo || 'page=1'
    searchValue = searchValue || ''
    commit('resetDebtors')
    debtorService.getDebtors(pageNo, searchValue).then(response => {
      commit('setDebtors', response.data)
      commit('setTableLoading', false)
    })
  },
  createDebtor({ state, commit, dispatch }) {
    commit("resetSetError")
    if (state.formInput.country_iso_code == "DK") {
      dispatch("accountFormValidationsDenmark")
    } else {
      dispatch("accountFormValidationsInternational")
    }
    return new Promise((resolve, reject) => {
      if (state.formInput.error === false) {
        commit("setButtonDisabled", true)
        dispatch("makePostObject")
        Vue.axios.post('/debtors', state.postObject).then(response => {
          notify(i18n.t('debtor.toastr_debtor'))
          dispatch("updateDebtorsState")
          commit("setButtonDisabled", false)
          resolve(response.data.data)
        })
          .catch(error => {
            commit("setButtonDisabled", false)
            reject(dispatch("showSignupError", error.response.data.data.errors))
          })
      } else {
        reject()
      }
    })
  },
  accountFormValidationsDenmark({ state, commit }) {
    if (state.formInput.is_company === 1 && (state.formInput.vat_number === null || (state.formInput.vat_number.toString()).length !== 8 || (state.formInput.vat_number.toString()).match(/^[0-9]+$/) == null)) { // not in case of private
      commit("setVatError")
    }
    if (state.formInput.is_company === 1 && (state.formInput.company_name === "" || state.formInput.company_name === null)) {
      commit("setCompanyNameError")
    }
    if (state.formInput.is_company === 0 && (state.formInput.first_name === "" || state.formInput.first_name === null)) {
      commit("setFirstNameError")
    }
    if (state.formInput.is_company === 0 && (state.formInput.last_name === "" || state.formInput.last_name === null)) {
      commit("setLastNameError")
    }
    if (state.formInput.address === "" || state.formInput.address === null) {
      commit("setAddressError")
    }
    if (state.formInput.zip_code === null || state.formInput.zip_code.length !== 4 || state.formInput.zip_code.match(/^[0-9]+$/) == null) {
      commit("setZipcodeError")
    }
    if (state.formInput.city === "" || state.formInput.city === null) {
      commit("setCityError")
    }
    if (state.formInput.phone === null || state.formInput.phone.length !== 8 || state.formInput.phone.match(/^[0-9]+$/) == null) {
      commit("setPhoneError")
    }
    if (state.formInput.email !== null && state.formInput.email !== "") {
      if (!validEmail(state.formInput.email)) {
        commit('setEmailError')
      }
    }
    if (state.formInput.email == null || state.formInput.email == "") {
      commit('setEmailError')
    }
  },
  accountFormValidationsInternational({ state, commit }) {
    if (state.formInput.is_company === 1 && (state.formInput.vat_number === null || (state.formInput.vat_number.toString()).match(/^[0-9]+$/) == null)) { // not in case of private
      commit("setVatError")
    }
    if (state.formInput.is_company === 1 && (state.formInput.company_name === "" || state.formInput.company_name === null)) {
      commit("setCompanyNameError")
    }
    if (state.formInput.is_company === 0 && (state.formInput.first_name === "" || state.formInput.first_name === null)) {
      commit("setFirstNameError")
    }
    if (state.formInput.is_company === 0 && (state.formInput.last_name === "" || state.formInput.last_name === null)) {
      commit("setLastNameError")
    }
    if (state.formInput.address === "" || state.formInput.address === null) {
      commit("setAddressError")
    }
    if (state.formInput.zip_code === null || state.formInput.zip_code.match(/^[0-9]+$/) == null) {
      commit("setZipcodeError")
    }
    if (state.formInput.city === "" || state.formInput.city === null) {
      commit("setCityError")
    }
    if (state.formInput.phone === null || state.formInput.phone.match(/^[0-9]+$/) == null || state.formInput.phone === "") {
      commit("setPhoneError")
    }
    if (state.formInput.email !== null && state.formInput.email !== "") {
      if (!validEmail(state.formInput.email)) {
        commit('setEmailError')
      }
    }
    if (state.formInput.email == null || state.formInput.email == "") {
      commit('setEmailError')
    }
  },
  makePostObject({ state, commit, rootState }) {
    state.postObject = {
      company_name: state.formInput.company_name,
      address: state.formInput.address,
      zip_code: state.formInput.zip_code,
      city: state.formInput.city,
      phone: state.formInput.phone,
      vat_number: state.formInput.vat_number,
      account_id: rootState.accounts.userProfile[0].active_account_id,
      first_name: state.formInput.first_name,
      last_name: state.formInput.last_name,
      email: state.formInput.email,
      is_company: state.formInput.is_company,
      country_id: state.formInput.country,
    }
  },
  showSignupError({ state, commit }, errorObject) {
    return commit('setSeverError', i18n.t("auth.something_went_wrong"))
  },
  fetchInfo({ state, commit }) {
    commit("setTableLoading", true)
    Vue.axios.get('/debtors/' + state.id + "?showCountries=1")
      .then(response => {
        commit("setEditState", response.data.data)
        commit("setTableLoading", false)
        commit('setFieldLimits')
      })
  },
  editDebtor({ state, commit, dispatch }) {
    commit("resetSetError")
    if (state.formInput.country_iso_code == "DK") {
      dispatch("accountFormValidationsDenmark")
    } else {
      dispatch("accountFormValidationsInternational")
    }
    return new Promise((resolve, reject) => {
      if (state.formInput.error === false) {
        commit("setButtonDisabled", true)
        dispatch("makePostObject")
        Vue.axios.put('/debtors/' + state.id, state.postObject).then(response => {
          notify(i18n.t('debtor.toastr_debtor_edit'))
          dispatch("updateDebtorsState")
          resolve(response)
        })
          .catch(error => {
            commit("setButtonDisabled", false)
            reject(dispatch("showSignupError", error.response.data.data.errors))
          })
      } else {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject()
      }
    })
  },
  updateDebtorsState({ dispatch, commit }) {
    commit("resetDebtors")
    dispatch("getDebtors")
  }
}
