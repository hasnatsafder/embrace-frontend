import $store from '../store'

export function checkAccessMiddleware (to, from, next) {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    let user = $store.getters.user
    if (!user) {
      next({
        path: `/login`,
        query: { redirect: '/login' }
      })
    }
    next()
  } else {
    next()
  }
}
