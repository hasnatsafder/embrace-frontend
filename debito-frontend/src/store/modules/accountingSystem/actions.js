import Vue from 'vue'
import { validEmail, notify } from '@/helpers'
import i18n from '@/plugins/i18n'
import router from '@/router'
import moment from 'moment'

export default {
  // to view current dunning on an invoice
  getInvoiceDunnings({ state, commit }, invoice_id) {
    commit('setLoading', true)
    commit('resetInvoiceDunnings')
    Vue.axios.post('/claim-dunnings/', { 'invoice_id': invoice_id }).then(response => {
      commit('setInvoiceDunnings', response.data.data)
      commit('setLoading', false)
    })
      .catch(error => {
        console.log(error)
      })
  },
  // to edit the invoice dunnings
  editInvoiceDunnings({ state, commit, dispatch }) {
    if (state.invoiceDunnings.warning_1) {
      state.invoiceDunnings.warning_1 = moment(state.invoiceDunnings.warning_1).format("YYYY-MM-DD")
    }
    if (state.invoiceDunnings.warning_2) {
      state.invoiceDunnings.warning_2 = moment(state.invoiceDunnings.warning_2).format("YYYY-MM-DD")
    }
    if (state.invoiceDunnings.warning_3) {
      state.invoiceDunnings.warning_3 = moment(state.invoiceDunnings.warning_3).format("YYYY-MM-DD")
    }
    commit('setLoading', true)
    dispatch('setDunningObj')
    Vue.axios.put("/claim-dunnings/" + state.invoiceDunnings.id, state.dunningObjects).then(response => {
      commit('setLoading', false)
      if (response.data.debtor_saved) {
        notify(i18n.t('debtor.email_edited'))
      }
      router.push('/accountingsystem/')
    })
      .catch(error => {
        console.log(error)
        commit('setLoading', false)
      })
  },
  unSyncAccount ({ commit }) {
    commit('setLoading', true)
    Vue.axios.get('/invoices/unSyncAccount').then(response => {
      if (response.data.success) {
        commit('setShowInvoices', false)
        commit('setVendor', null)
        commit('resetInvoices')
      }
      commit('setLoading', false)
      commit('setVendor', null)
    }).catch(error => {
      console.log(error)
      commit('setLoading', false)
    })
  },
  refreshAccountSync ({ commit, dispatch }) {
    commit('setErrorMessage', false)
    commit('setLoading', true)
    return Vue.axios.get('/invoices/refreshAccountSync').then(response => {
      if (response.data.success) {
        dispatch('checkSynced', response.data.data)
        commit('setLoading', false)
      }
    }).catch(error => {
      console.error(error)
      commit('setErrorMessage', true)
      commit('setShowInvoices', true)
      commit('setLoading', false)
    })
  },
  integrationCheck ({ commit }) {
    commit('setAccountingSystemConnected', null)
    commit('setErrorMessage', false)
    commit('setLoading', true)
    commit('resetInvoicesPage')
    commit('resetVendor')
    Vue.axios.get('/invoices/integrationcheck').then(response => {
      if (response.data.success) {
        commit('setShowInvoices', true)
        commit('setAccountingSystemConnected', true)
        commit('setVendor', response.data.data)
      }
      commit('setLoading', false)
    }).catch(error => {
      console.error(error)
      commit('setShowInvoices', false)
      commit('setAccountingSystemConnected', false)
      commit('setLoading', false)
    })
  },
  getEconomicStatus ({ commit }) {
    commit('setLoading', true)
    Vue.axios.get('/integrations/economic/status').then(response => {
      if (response.data.success) {
        console.log(response.data.success)
        commit('setShowInvoices', true)
      }
      commit('setLoading', false)
    }).catch(error => {
      console.error(error)
      commit('setShowInvoices', false)
      commit('setLoading', false)
    })
  },
  getEconomicUrl ({ commit }) {
    Vue.axios.post('/integrations/economic/start-auth').then(response => {
      let win = window.open(response.data.data.redirect_url, '_blank')
      win.focus()
    }).catch(error => {
      console.error(error)
      commit('setLoading', false)
    })
  },
  getInvoices ({ state, commit, dispatch }, perPage, searchTerm) {
    commit('setAnimatedLoader', true)
    perPage = perPage || '&page=1'
    searchTerm = searchTerm || ''
    return Vue.axios.get('/invoices/myInvoices?' + perPage + searchTerm).then(response => {
      if (response.data.success) {
        commit('setInvoices', response.data)
        commit('setAnimatedLoader', false)
        this.dispatch('debtors/getDebtors')
      }
      return response
    }).catch(error => {
      commit('setAnimatedLoader', false)
      console.error(error)
    })
  },
  async editInvoice({ state, dispatch }) {
    for (let i = 0; i < state.selectedInvoices.length; i++) {
      state.selectedInvoices[i].due_date = moment(state.selectedInvoices[i].due_date).format("YYYY-MM-DD")
      state.selectedInvoices[i].issue_date = moment(state.selectedInvoices[i].issue_date).format("YYYY-MM-DD")
      await Vue.axios.put('/invoices/' + state.selectedInvoices[i].id, state.selectedInvoices[i]).then(response => {
        Vue.axios.put('/debtors/' + state.selectedInvoices[i].debtor_id, state.selectedInvoices[i].Debtors).then(response => {
          return response
        })
      })
    }
  },
  addFromInvoices({ state, dispatch }, data) {
    let invoice_array = {
      "invoice_ids": []
    }
    for (let i = 0; i < data.selectedInvoices.length; i++) {
      invoice_array["invoice_ids"].push(data.selectedInvoices[i].id)
      if (data.dunning_configuration_id) {
        invoice_array["dunning_configuration_id"] = data.dunning_configuration_id
      }
    }
    return Vue.axios.post('/claims/add-from-invoices', invoice_array)
  },
  billyAuth({ state, commit }) {
    commit('resetSetError')
    if (!validEmail(state.formInput.email)) {
      commit('setEmailError')
    }
    if (state.formInput.password === null || state.formInput.password === '') {
      commit('setPasswordError')
    }
    if (state.formInput.error === false) {
      commit("setButtonDisabled", true)
      Vue.axios.post('/integrations/billy/start-auth', {
        billy_email: state.formInput.email,
        billy_password: state.formInput.password
      }).then(response => {
        commit("setButtonDisabled", true)
        if (response.data.success) {
          Vue.axios.post('/integrations/billy/finish-auth', {
            billy_email: state.formInput.email,
            billy_password: state.formInput.password,
            organizationId: response.data.data[0].organizationId,
          }).then(response => {
            // if account sync successful, send to sync page
            router.push('/accountingsystem/syncing/' + response.data.data.reference)
          })
        }
      }).catch(error => {
        console.error(error)
        commit("setButtonDisabled", false)
        commit('setSeverError', i18n.t("auth.incorrect_email_password"))
      })
    }
  },
  dineroAuthStart({ state, commit, dispatch }) {
    commit('resetSetError')
    dispatch("apiKeyValidation")
    if (state.formInput.error === false) {
      commit('setLoading', true)
      Vue.axios.post('/integrations/dinero/start-auth', {
        api_key: state.formInput.api_key,
      }).then(response => {
        commit("setOrganization", response.data)
        // set first organization as default
        commit('setLoading', false)
        if (state.formInput.organizations.data[0]) {
          commit("setSelectedOrganization", state.formInput.organizations.data[0].id)
        }
      }).catch(() => {
        commit('setLoading', false)
        commit("setButtonDisabled", false)
        commit('setSeverError', i18n.t("erpIntegration.incorrect_api"))
      })
    }
  },
  dineroAuthFinish({ state, commit }) {
    commit('setLoading', true)
    Vue.axios.post('/integrations/dinero/finish-auth', {
      api_key: String(state.formInput.api_key),
      org_id: String(state.formInput.selectedOrganization)
    }).then(response => {
      // if account sync successful, send to sync page
      router.push('/accountingsystem/syncing/' + response.data.data.reference)
    }).catch(() => {
      console.log("error")
      commit('setLoading', false)
      commit("setButtonDisabled", false)
      commit('setSeverError', i18n.t("erpIntegration.incorrect_api"))
    })
  },
  apiKeyValidation({ state, commit }) {
    if (state.formInput.api_key === null) {
      commit("setApiError")
    }
  },
  snoozeInvoice({ state, commit }, payload) {
    return Vue.axios.put(`/invoices/${payload}`, {
      "snoozed": 1
    }).then(response => {
      return response
    })
  },
  unSnoozeInvoice({ state, commit }, payload) {
    return Vue.axios.put(`/invoices/${payload}`, {
      "snoozed": 0
    })
  },
  setDunningObj({ state }) {
    state.dunningObjects = {
      debtor_email: state.invoiceDunnings.debtor_email,
      warning_1: state.invoiceDunnings.warning_1,
      warning_2: state.invoiceDunnings.warning_2,
      warning_3: state.invoiceDunnings.warning_3,
      cc_email: state.invoiceDunnings.cc_email,
      bcc_email: state.invoiceDunnings.bcc_email,
      email_text: state.invoiceDunnings.email_text,
      warning_1_amount: state.invoiceDunnings.warning_1_amount,
      warning_2_amount: state.invoiceDunnings.warning_2_amount,
      warning_3_amount: state.invoiceDunnings.warning_3_amount,
    }
  },
  fetchInfo({ state, commit }, id) {
    Vue.axios.get('/invoices/' + id)
      .then(response => {
        commit("setSingleInvoice", response.data)
      })
  },
  checkSynced({ state, commit }, reference) {
    commit("setsyncing", true)
    notify(i18n.t('erpIntegration.syncing_started'), 5000000, "info", "")
    let polling = setInterval(() => {
      Vue.axios
        .post("/invoices/checkQueue", {
          reference: reference
        })
        .then(response => {
          if (response.data.data.finished == "true" || response.data.data.finished == true) {
            commit("setsyncing", false)
            notify(i18n.t('erpIntegration.intergration_completed'), 3000, "success", "accountingsystem/")
            clearInterval(polling)
          }
        })
        .catch(error => {
          commit("setsyncing", false)
          console.log(error)
        })
    }, 2000)
  },
  getStatsData({ commit }, account_id) {
    commit("resetStatsData")
    Vue.axios.get('/invoices/invoicesMonthlyStats/' + account_id)
      .then(response => {
        commit("setStatsData", response.data)
      })
  }
}
