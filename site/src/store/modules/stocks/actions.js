import Vue from 'vue'
// import store from '@/store/index'

export default {
  // get all stocks
  getStocks({commit}) {
    Vue.axios.get('/stocks').then(response => (
      commit("setStocks", response)
    ))
  },

  // get history of a stock
  getHistory({commit}, symbol) {
    return Vue.axios.get('/stocks/' + symbol + '/120').then(response => {
      commit("setHistoryData", response)
    })
  },

  // get averages of a stock
  getAverages({commit}) {
    return Vue.axios.get('/stock_averages/30').then(response => {
      commit("setAveragesData", response)
    })
  },

  getStock({commit}, id) {
    return Vue.axios.get("/stock-detail/" + id).then(response => {
      commit("setStockDetail", response)
    })
  }
}