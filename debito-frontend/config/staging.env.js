'use strict'
module.exports = {
  NODE_ENV: '"production"',
  ROOT_API: '"https://stagingadmin.debito.dk/api/v1"',
  ROOT_ADDRESS: '"https://stagingadmin.debito.dk/"'
}
