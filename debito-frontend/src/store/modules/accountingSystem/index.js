import actions from './actions.js'
import mutations from './mutations.js'
import { renderSpinner } from '@/helpers'

export default {
  namespaced: true,
  state: {
    animatedLoader: false,
    errorMessage: false,
    accountingSystemConnected: null,
    deleted: false,
    loading: false,
    showInvoices: false,
    statsData: {},
    formInput: {
      api_key: null, // for dinero
      email: null,
      password: null,
      serverError: false,
      buttonDisabled: false,
      error: false,
      organizations: [],
      selectedOrganization: ""
    },
    styleObject: {
      errorObject: {
        email: null,
        password: null,
        api_key: null
      }
    },
    vendor: null,
    invoices: [],
    selectedInvoices: [],
    sameDebtor: [],
    invoiceDunnings: [],
    singleInvoice: [],
    syncing: false,
  },
  getters: {
    buttonSpinner: (state, fontSize) => {
      return renderSpinner(fontSize)
    }
  },
  actions,
  mutations,
}
