import '@mdi/font/css/materialdesignicons.css'
import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdi',
  },
  theme: {
    options: {
      customProperties: true
    },
    dark: false,
    themes: {
      light: {
        primary: '#141A46',
        secondary: '#EC8B5E',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#18C5A9',
        'success-hover': "#005500",
        warning: '#FFC107',
      },
    },
  },
  
});
