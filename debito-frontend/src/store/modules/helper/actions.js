import Vue from 'vue'

export default {
  searchAccountForName({ state, commit }, payload) {
    return Vue.axios.post('/accounts/cvrsearch', {
      type: 'any',
      term: payload
    })
      .then(response => {
        return response
      })
      .catch(error => {
        console.error(error)
        return error
      })
  },
  searchAccountForVat({ state, commit }, payload) {
    return Vue.axios.post('/accounts/cvrsearch', {
      type: 'vat',
      term: parseInt(payload)
    })
      .then(response => {
        return response
      })
      .catch(error => {
        console.error(error)
        return error
      })
  }
}
