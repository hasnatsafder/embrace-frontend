import Vue from 'vue'

export default {

  // set an active account of user
  setActiveAccount (account_id, user_id) {
    return Vue.axios.put('/users/' + user_id, {
      active_account_id: account_id
    }).then(response => {
      return response.data
    })
  },

  // get user profiles
  getUserProfile() {
    return Vue.axios.get('/users').then(response => {
      return response
    })
  }
}
