// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './pages/App'
import Router from './router'
import Auth from './plugins/Auth'
import store from './store/index'
import i18n from './plugins/i18n'

import './builder/styleBuilder'
import './router/middleware'

Vue.use(Auth)

Vue.config.productionTip = false
export default Vue

export const bus = new Vue()

new Vue({
  el: '#app',
  router: Router,
  i18n,
  template: '<App/>',
  store: store,
  render: h => h(App)
})
i18n.locale = localStorage.getItem('selectedLanguage') ? localStorage.getItem('selectedLanguage') : "da"
