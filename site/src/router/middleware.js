import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios)

axios.interceptors.request.use((request) => {
  if (request.url[0] === '/') {
    request.headers = { "Content-Type": "application/json", "Accept": "application/json", "Access-Control-Allow-Origin": '*' }
    request.url = process.env.VUE_APP_API_URL + request.url
    request.crossDomain = true
  }
  return request
})