// import Home from '../pages/Home.vue'
import Stocks from '../pages/Stocks.vue'
import Averages from '../pages/Averages.vue'
import History from '../pages/History.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Stocks
  },
  {
    path: '/stocks',
    name: 'stocks',
    component: Stocks
  },
  {
    path: '/averages',
    name: 'averages',
    component: Averages
  },
  {
    path: '/stocks/history/:symbol',
    name: 'history',
    props: true,
    component: History
  },
]

export default routes