import { validEmail } from '@/helpers'
import router from '@/router/index'
import Vue from 'vue'
import store from '@/store/index'
import i18n from '@/plugins/i18n'
const REMEMBER_TOKEN_TIME = 604800000
const REMEMBER_NOT_TOKEN_TIME = 14400000

export default {
  signIn ({ state, commit, dispatch }) {
    commit('resetSetError')
    if (!validEmail(state.formInput.email)) {
      commit('setEmailError')
    }
    if (state.formInput.password === null || state.formInput.password === '') {
      commit('setPasswordError')
    }
    if (state.formInput.error === false) {
      new Promise((resolve, reject) => {
        commit("setButtonDisabled", true)
        Vue.axios.post('/auth/token', {
          email: state.formInput.email.toLowerCase(), //
          password: state.formInput.password
        }).then(response => {
          if (state.formInput.rememberMe) {
            resolve(Vue.auth.setToken(response.data.data.token, Date.now() + REMEMBER_TOKEN_TIME))
          } else {
            resolve(Vue.auth.setToken(response.data.data.token, Date.now() + REMEMBER_NOT_TOKEN_TIME))
          }
          resolve(Vue.auth.setIntercomInfo(state.formInput.email))
          store.commit('claims/resetClaimsState')
          store.commit('debtors/resetDebtors')
          store.commit('accountingSystem/syncing', false)
          commit('resetPasswordAndEmail')
          let redirectUrl = window.location.href.split('%2F')
          redirectUrl = redirectUrl.slice(1, redirectUrl.length).join().replace(/,/g, "/")
          router.push("/" + redirectUrl)
        })
          .catch(error => {
            commit("setButtonDisabled", false)
            reject(commit('setSeverError', i18n.t("auth.incorrect_email_password")))
          })
      })
    }
  },
  signUp ({ state, commit, dispatch }) {
    commit('resetSetError')
    if (!validEmail(state.formInput.email)) {
      commit('setEmailError')
    }
    if (state.formInput.password === null || state.formInput.password.length < 6) {
      commit('setPasswordError')
    }
    if (state.formInput.phone === null || state.formInput.phone.length < 8 || state.formInput.phone === "") {
      commit('setPhoneError')
    }
    if (state.formInput.error === false) {
      new Promise((resolve, reject) => {
        commit("setButtonDisabled", true)
        Vue.axios.post('/auth/register', {
          email: state.formInput.email.toLowerCase(),
          password: state.formInput.password,
          phone: state.formInput.phone
        }).then(response => {
          resolve(Vue.auth.setToken(response.data.data.token, Date.now() + REMEMBER_NOT_TOKEN_TIME))
          resolve(Vue.auth.setIntercomInfo(state.formInput.email))
          store.commit('claims/resetClaimsState')
          store.commit('debtors/resetDebtors')
          store.commit('accountingSystem/syncing', false)
          commit('resetPasswordAndEmail')
          router.push("/createaccount")
        })
          .catch(error => {
            commit("setButtonDisabled", false)
            reject(dispatch("showSignupError", error.response.data.data.errors))
          })
      })
    }
  },
  // setIntercomInformation({state}){}
  forgetPassword({ state, commit, dispatch }) {
    commit('resetSetError')
    if (!validEmail(state.formInput.email)) {
      commit('setEmailError')
    }
    if (state.formInput.error === false) {
      new Promise((resolve, reject) => {
        commit("setButtonDisabled", true)
        Vue.axios.post('/auth/forgotpassword', {
          email: state.formInput.email
        }).then(response => {
          if (response.data.success) {
            resolve(commit('setSeverSuccess', i18n.t("auth.forgot_email_sent")))
          } else {
            resolve(commit('setSeverError', i18n.t("auth.forgot_incorrect_Email")))
          }
          commit("setButtonDisabled", false)
        })
          .catch(error => {
            commit("setButtonDisabled", false)
            reject(commit('setSeverError', i18n.t("auth.something_went_wrong")))
          })
      })
    }
  },
  verify({ state, commit }) {
    Vue.axios.get('/auth/verify/' + state.formInput.token, {
    }).then(response => {
      if (response.data.success) {
        commit("setTokenVerified", true)
      } else {
        router.push("/forgotpassword?error=token-error")
      }
    }).catch(error => {
      router.push("/forgotpassword?error=token-error")
    })
  },
  resetPassword({ state, commit }) {
    commit("setButtonDisabled", true)
    Vue.axios.post("/auth/resetpassword/" + state.formInput.token, {
      password: state.formInput.password
    }).then(response => {
      if (response.data.success) {
        commit('setSeverSuccess', i18n.t("auth.password_reset_successfull"))
        commit("setButtonDisabled", false)
        commit("resetPasswordAndEmail")
      }
    }).catch(error => {
      commit('setSeverError', i18n.t("auth.password_reset_failed"))
      commit("setButtonDisabled", false)
    })
  },
  showSignupError({ state, commit }, errorObject) {
    try {
      if (errorObject.email) {
        if (errorObject.email._isUnique) {
          return commit('setSeverError', i18n.t("auth.email_already_taken"))
        } else {
          return commit('setSeverError', i18n.t("auth.provide_valid_email"))
        }
      } else if (errorObject.password) {
        return commit('setSeverError', i18n.t("auth.provide_valid_password_register"))
      }
    } catch (err) {
      return commit('setSeverError', i18n.t("auth.something_went_wrong"))
    }
  },
  getUserInfo({ state, commit }) {
    state.userInfoLoading = true
    return Vue.axios.get('/users')
      .then(response => {
        let userInfo = response.data.data[0]
        commit('setUserInfo', userInfo)
        state.userInfoLoading = false
        return userInfo
      })
      .catch(error => {
        state.userInfoLoading = false
        return error
      })
  },
  updateUserInfo({ state, commit }, payload) {
    state.formInput.infoButtonDisabled = true
    return Vue.axios.put(`/users/${payload}`, {
      first_name: state.formInput.first_name,
      last_name: state.formInput.last_name,
      phone: state.formInput.phone,
      email: state.formInput.email
    })
      .then(response => {
        state.formInput.infoButtonDisabled = false
        return response
      })
      .catch(error => {
        state.formInput.infoButtonDisabled = false
        return error
      })
  },
  updateUserPassword({ state, commit }, payload) {
    state.formInput.buttonDisabled = true
    return Vue.axios.put(`/users/${payload.id}`, {
      old_password: payload.oldPassword,
      password: payload.newPassword
    })
      .then(response => {
        if (response.data.success) {
          state.formInput.buttonDisabled = false
          return true
        } else {
          state.formInput.buttonDisabled = false
        }
      }).catch(error => {
        state.formInput.buttonDisabled = false
        return error
      })
  },
}
