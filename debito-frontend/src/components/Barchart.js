import { Bar } from 'vue-chartjs'

export default {
  extends: Bar,
  data: () => ({
    chartdata: {
      labels: ['January', 'February', 'March', 'April', 'May', 'June'],
      datasets: [
        {
          label: 'Paid Invoices',
          backgroundColor: '#2BBEBB',
          data: [2000, 2000, 1000, 2000, 2500, 3000]
        },
        {
          label: 'Unpaid Invoices',
          backgroundColor: '#5DD0FC',
          data: [2000, 4000, 2000, 3000, 4000, 3500]
        }
      ]
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        yAxes: [
          {
            stacked: true,
            barPercentage: 0.4,
            ticks: {
              max: 5000,
              min: 0,
              stepSize: 5000,
              beginAtZero: true,
              padding: 0,
              callback: function(value, index, values) {
                return value / 1000 + "k"
              }
            },
            // scaleLabel: {
            //   display: true,
            //   // labelString: '1k = 1000'
            // }
          }
        ],
        xAxes: [{
          stacked: true,
        }],
      }
    }
  }),

  mounted () {
    this.renderChart(this.chartdata, this.options)
  }
}
