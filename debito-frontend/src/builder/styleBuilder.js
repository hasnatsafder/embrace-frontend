import Vue from 'vue'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import '../assets/css/bulma-changes.scss'
import VueAlertify from 'vue-alertify'
import Toastr from '@/components/Toastr'
import BarLoader from '@/components/BarLoader.vue'
import arrowleft from '@/components/svg/arrow-left.vue'
import arrowright from '@/components/svg/arrow-right.vue'
import Loader from '@/components/svg/spinner-solid.vue'
import Loader2 from '@/components/svg/loader-2.vue'
import syncIcon from '@/components/svg/sync-icon.vue'
import plusIcon from '@/components/svg/plus-icon.vue'
import bellIcon from '@/components/svg/bell-icon.vue'
import iconEyeSlash from '@/components/svg/iconEyeSlash.vue'
import iconEye from '@/components/svg/iconEye.vue'
import accountIconSidebar from '@/components/svg/sidebar/accounts-icon.vue'
import dashboardIconSidebar from '@/components/svg/sidebar/dashboard-icon.vue'
import debtorIconSidebar from '@/components/svg/sidebar/debtors-icon.vue'
import claimsIconSidebar from '@/components/svg/sidebar/claims-icon.vue'
import settingsIconSidebar from '@/components/svg/sidebar/settings-icon.vue'
import usersIconSidebar from '@/components/svg/sidebar/user-details.vue'
import reminderIcon from '@/components/svg/reminder-envelop.vue'
import manageAccounts from '@/components/svg/manage-accounts.vue'
import userNotification from '@/components/svg/user-notification.vue'
import phoneIcon from '@/components/svg/phone-icon.vue'
import editIcon from '@/components/svg/edit-icon.vue'
import editWhiteIcon from '@/components/svg/edit-white-icon.vue'
import arrowDown from '@/components/svg/arrow-down.vue'
import arrowUp from '@/components/svg/arrow-up.vue'
// Eva icons
import evaDashboard from '@/components/svg/eva/eva-dashboard.vue'
import evaClaims from '@/components/svg/eva/eva-claims.vue'
import evaDebtors from '@/components/svg/eva/eva-debtors.vue'
import evaInvoice from '@/components/svg/eva/eva-invoice.vue'
import evaEdit from '@/components/svg/eva/eva-edit.vue'
import evaDelete from '@/components/svg/eva/eva-delete.vue'
import evaLegalCard from '@/components/svg/eva/eva-legal-card.vue'
import evaReminderCard from '@/components/svg/eva/eva-reminder-card.vue'
import evaUnpaidCard from '@/components/svg/eva/eva-unpaid-card.vue'
import evaClaimCard from '@/components/svg/eva/eva-claim-card.vue'
import evaSettings from '@/components/svg/eva/eva-settings.vue'
import evaAccounts from '@/components/svg/eva/eva-accounts.vue'
import evaArrowright from '@/components/svg/eva/eva-arrowright.vue'
import evaArrowleft from '@/components/svg/eva/eva-arrowleft.vue'
import fileIcon from '@/components/svg/file-icon.vue'

import '../assets/css/spacing.scss'
import '../assets/css/font-sizes.scss'
import '../assets/css/main-new.scss'

// font awesome
import { library } from '@fortawesome/fontawesome-svg-core'
// internal icons
import { faCheck, faCheckCircle, faInfoCircle, faExclamationTriangle, faExclamationCircle,
  faArrowUp, faAngleRight, faAngleLeft, faAngleDown, faAngleUp, faUser, faLink, faCog,
  faCashRegister, faCalculator, faAdjust,
  faEye, faEyeSlash, faCaretDown, faCaretUp } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome"

library.add(faCheck, faCheckCircle, faInfoCircle, faExclamationTriangle, faExclamationCircle,
  faArrowUp, faAngleRight, faAngleLeft, faAngleDown, faAngleUp, faUser, faLink, faCog,
  faCashRegister, faCalculator, faAdjust,
  faEye, faEyeSlash, faCaretDown, faCaretUp)

Vue.use(Buefy, {
  defaultIconComponent: 'vue-fontawesome',
  defaultIconPack: 'fas',
})
Vue.use(VueAlertify)

Vue.component('BarLoader', BarLoader)
Vue.component('vue-fontawesome', FontAwesomeIcon)
Vue.component('arrowleft', arrowleft)
Vue.component('arrowright', arrowright)
Vue.component('syncIcon', syncIcon)
Vue.component('plusIcon', plusIcon)
Vue.component('bellIcon', bellIcon)
Vue.component('iconEyeSlash', iconEyeSlash)
Vue.component('iconEye', iconEye)
Vue.component('Loader', Loader)
Vue.component('Loader2', Loader2)
Vue.component('accountIconSidebar', accountIconSidebar)
Vue.component('dashboardIconSidebar', dashboardIconSidebar)
Vue.component('debtorIconSidebar', debtorIconSidebar)
Vue.component('claimsIconSidebar', claimsIconSidebar)
Vue.component('settingsIconSidebar', settingsIconSidebar)
Vue.component('reminderIcon', reminderIcon)
Vue.component('manageAccounts', manageAccounts)
Vue.component('userNotification', userNotification)
Vue.component('phoneIcon', phoneIcon)
Vue.component('usersIconSidebar', usersIconSidebar)
Vue.component('editIcon', editIcon)
Vue.component('Toastr', Toastr)
Vue.component('editWhiteIcon', editWhiteIcon)
Vue.component('arrowDown', arrowDown)
Vue.component('arrowUp', arrowUp)
Vue.component('evaDashboard', evaDashboard)
Vue.component('evaClaims', evaClaims)
Vue.component('evaDebtors', evaDebtors)
Vue.component('evaInvoice', evaInvoice)
Vue.component('evaEdit', evaEdit)
Vue.component('evaDelete', evaDelete)
Vue.component('evaLegalCard', evaLegalCard)
Vue.component('evaReminderCard', evaReminderCard)
Vue.component('evaUnpaidCard', evaUnpaidCard)
Vue.component('evaClaimCard', evaClaimCard)
Vue.component('evaSettings', evaSettings)
Vue.component('evaAccounts', evaAccounts)
Vue.component('evaArrowright', evaArrowright)
Vue.component('evaArrowleft', evaArrowleft)
Vue.component('fileIcon', fileIcon)
