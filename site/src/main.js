import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'
import Buefy from 'buefy'
import router from './router'
import store from './store/index'
import './router/middleware'
import './assets/scss/app.scss'

Vue.use(Buefy)
Vue.use(Vuex)
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app')
