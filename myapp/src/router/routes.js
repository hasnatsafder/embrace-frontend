import HomePage from '@/views/Home.vue'
import LoginPage from '@/views/Authentication/Login.vue'
import NotFoundPage from '../views/NotFound.vue'

export const routes = [
  {
    path: '/',
    name: 'hello',
    component: HomePage,
    meta: {
      title: 'Home',
      requiresAuth: true,
    }
  },
  {
    path: '*',
    component: NotFoundPage,
    meta: { title: 'not found' }
  },
  {
    path: '/login',
    name: 'login',
    component: LoginPage,
    meta: { title: 'Home' }
  },
]
