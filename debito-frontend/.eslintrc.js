module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    "global-require": 0,
    // semi: [2, "always"],
    camelcase: 'off',
    'no-new': 'off',
    'space-before-function-paren': 'off',
    'quotes': 'off',
    'eqeqeq': 'off',
    'comma-dangle': 'off'
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
