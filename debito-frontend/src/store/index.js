import Vue from 'vue'
import auth from './modules/auth'
import accounts from './modules/accounts'
import debtors from './modules/debtors'
import claims from './modules/claims'
import helper from './modules/helper'
import accountingSystem from './modules/accountingSystem'
import Vuex from 'vuex'

Vue.use(Vuex)
export default new Vuex.Store({
  modules: {
    auth,
    accounts,
    debtors,
    claims,
    accountingSystem,
    helper
  }
})
