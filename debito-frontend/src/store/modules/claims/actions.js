import Vue from "vue"

export default {
  getClaims({ state, commit }) {
    commit("resetClaims")
    commit("setTableLoading", true)
    Vue.axios
      .get("/claims?includeClaimLineSum=1")
      .then(response => {
        commit("setClaims", response.data)
        commit("setTableLoading", false)
      })
      .catch(error => {
        commit("setTableLoading", false)
        console.error(error)
      })
  },
  countClaims({ state, commit }) {
    commit("resetClaimsCount")
    return new Promise((resolve, reject) => {
      Vue.axios
        .get("/claims?count=1")
        .then(response => {
          commit("setClaimsCount", response.data)
          resolve()
        })
        .catch(error => {
          reject()
        })
    })
  },
  makeClaim({ state, commit, dispatch }) {
    return new Promise((resolve, reject) => {
      Vue.axios({
        method: "post",
        url: "/claims",
        data: {
          debtor_id: state.selectedDebtor,
          dispute: state.dispute,
          currency_id: state.currentCurrency
        }
      })
        .then(response => {
          // dispatch("updateClaimsState")
          resolve(response.data.data)
        })
        .catch(error => {
          reject()
        })
    })
  },
  async makeClaimLines({ state }, formData) {
    console.log("formData ", formData)
    return new Promise((resolve, reject) => {
      Vue.axios({
        method: "post",
        url: `/claims/${state.claim_id}/claim-lines`,
        headers: {
          "Content-Type": "multipart/form-data"
        },
        data: formData
      })
        .then(response => {
          resolve(response.data.data)
        })
        .catch(error => {
          reject(error)
        })
    })
    // return await "made";
  },
  redirectToLandingPage() {
    window.location.href = "ClaimLandingPage"
  },
  async getClaimPhases({ state, commit }) {
    commit("resetClaimPhases")
    return new Promise((resolve, reject) => {
      Vue.axios
        .get("/claim-phases")
        .then(response => {
          commit("setClaimPhases", response.data)
          resolve()
        })
        .catch(error => {
          reject()
        })
    })
  },
  async getClaimLineTypes({ state, commit }) {
    return new Promise((resolve, reject) => {
      Vue.axios
        .get("/claim-line-types")
        .then(response => {
          commit("setclaimLineTypes", response.data)
          resolve()
        })
        .catch(error => {
          reject()
        })
    })
  },
  async getSingleClaim({ state, commit }, claim_id) {
    return new Promise((resolve, reject) => {
      Vue.axios
        .get("/claims/" + claim_id + "?showNames=1")
        .then(response => {
          commit("setSingleClaim", response.data)
          resolve()
        })
        .catch(error => {
          reject()
        })
    })
  },
  async getClaimLines({ state, commit }, claim_id) {
    return new Promise((resolve, reject) => {
      Vue.axios
        .get("/claims/" + claim_id + "/claim-lines?showNames=1")
        .then(response => {
          commit("setSingleClaimLines", response.data)
          resolve()
        })
        .catch(error => {
          reject()
        })
    })
  },
  async getClaimActions({ state, commit }, claim_id) {
    return new Promise((resolve, reject) => {
      Vue.axios
        .get(
          "/claims/" +
            claim_id +
            "/claim-actions?showNames=1&sort=date&limit=500"
        )
        .then(response => {
          commit("setClaimActions", response.data)
          resolve()
        })
        .catch(error => {
          reject()
        })
    })
  },
  async getClaimActionTypes({ state, commit }) {
    return new Promise((resolve, reject) => {
      Vue.axios
        .get("/claim-action-types?limit=200")
        .then(response => {
          commit("setClaimActionTypes", response.data)
          resolve()
        })
        .catch(error => {
          reject()
        })
    })
  },
  updateClaimsState({ dispatch, commit }) {
    commit("resetClaimsCount")
    dispatch("getClaims")
  },
  async getClaimFinancials({ state, commit }, claim_id) {
    commit("resetClaimFinancials")
    return new Promise((resolve, reject) => {
      Vue.axios
        .get("/claims/" + claim_id + "/claim-financials")
        .then(response => {
          commit("setClaimFinancials", response.data)
          resolve()
        })
        .catch(error => {
          reject()
        })
    })
  },
  getCurrencies({ state, commit }) {
    Vue.axios
      .get("/currencies?sort=name")
      .then(response => {
        commit("setCurrencies", response.data)
        // set DKK as default currency
        let danishKrone = response.data.data.filter(d => d.iso_code == "DKK")
        commit("setCurrentCurrency", danishKrone[0].id)
      })
      .catch(error => {})
  },
  async addDebtorLawyer({ state, commit }, formData) {
    if (state.debtorLawyer) {
      return Vue.axios({
        method: "post",
        url: '/debtor-lawyers',
        data: formData
      })
        .then(response => {
          commit("resetLawyerInput")
          return response
        })
        .catch(error => {
          console.error("[ERROR]: While adding debtor's lawyer", error)
        })
    } else {
      console.warn('Lawyer not created, insuffcient information')
      return false
    }
  },
  getAllClaimActions({ state, commit }) {
    commit("resetAllClaimActions")
    Vue.axios
      .get("/claims/allClaimActions/")
      .then(response => {
        commit("setAllClaimActions", response.data)
      })
      .catch(error => {})
  },
}
